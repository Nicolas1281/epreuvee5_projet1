<?php
require_once('includes/header.php');
require_once('includes/navbar.php');
?>

<div class="d-flex flex-lg-row-reverse align-items-center pb-5 pad">
    <div class="col-10 col-sm-8 col-lg-6">
        <img src="/assets/img/image1.jpg" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
    </div>
    <div class="col-lg-6">
        <h1 class="Colorh1 display-5 fw-bold text-center lh-1 mb-3">Bienvenue sur MilMedCare</h1>
        <h2 class="Colorh2 text-center">Un soutien médical pour nos forces armées</h2>
        <p class="colorp lead text-center mt-5">Chez MilMedcare, nous nous engageons à soutenir nos militaires en leur fournissant les meilleurs services de santé
            et les informations les plus à jour sur le terrain. Notre plateforme numérique a été conçue pour répondre aux besoins spécifiques de ceux qui servent
            notre nation, en les aidant à rester en forme, à rester en sécurité et à accomplir leurs missions avec succès.
        </p>
    </div>
</div>

<div class="d-flex flex-lg-row align-items-center pb-5 pad">
    <div class="col-10 col-sm-8 col-lg-6">
        <img src="/assets/img/image2.jpg" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
    </div>
    <div class="col-lg-6">
        <h2 class="Colorh2 text-center">Pourquoi MilMedCare ?</h2>
        <ul class="colorp lead mt-5">
            <li><strong>Performance Améliorée :</strong> En prenant soin de votre santé, vous êtes plus apte à accomplir vos missions avec succès.
                MilMedcare vous aide à rester en forme et en bonne santé, ce qui se traduit par une meilleure performance.
            </li>
            <br>
            <li><strong>Coordination simplifiée :</strong> Notre plateforme facilite la coordination entre les équipes médicales, les médecins sur le terrain et les centres de soins, assurant ainsi une prise en charge médicale efficace.</li>
            <br>
            <li><strong>Suivi personnalisé :</strong> MilMedcare permet de créer des dossiers médicaux individuels pour chaque soldat, garantissant un suivi médical précis et adapté à chaque personne.</li>
        </ul>
    </div>
</div>

<div class="d-flex flex-lg-row-reverse align-items-center pb-5 pad">
    <div class="col-10 col-sm-8 col-lg-6">
        <img src="/assets/img/image3.jpg" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
    </div>
    <div class="col-lg-6">
        <h2 class="Colorh2 text-center">Nos Fonctionnalités clés</h2>
        <ul class="colorp lead mt-5">
            <li><strong>Prise de rendez-vous en ligne :</strong> Les militaires peuvent aisément planifier leurs rendez-vous médicaux en quelques clics, réduisant ainsi les temps d'attente et optimisant leur temps sur le terrain.</li>
            <br>
            <li><strong>Consultation des réservations :</strong> Les militaires peuvent accéder facilement à leurs réservations médicales en tout temps.</li>
            <br>
            <li><strong>Diagnostics et résultats :</strong> MilMedcare simplifie l'enregistrement des diagnostics et des résultats des examens médicaux, ce qui facilite la prise de décisions médicales éclairées.</li>
            <br>
            <li><strong>Gestion des antécédents médicaux :</strong> Les soldats peuvent mettre à jour leurs antécédents médicaux, garantissant ainsi une prise en charge médicale adaptée à leurs besoins spécifiques.</li>
        </ul>
    </div>
</div>

<div class="d-flex flex-lg-row align-items-center pb-5 pad">
    <div class="col-10 col-sm-8 col-lg-6">
        <img src="/assets/img/image4.jpg" class="d-block mx-lg-auto img-fluid" alt="Bootstrap Themes" width="700" height="500" loading="lazy">
    </div>
    <div class="col-lg-6">
        <h2 class="Colorh2 text-center">La sécurité avant tout !</h2>
        <p class="colorp lead mt-5">Chez MilMedcare, la sécurité des données médicales de nos militaires est une priorité absolue. Notre plateforme est dotée de systèmes de sécurité avancés et de protocoles de cryptage pour assurer la confidentialité et l'intégrité des informations médicales. Votre confiance est notre engagement, et nous prenons des mesures strictes pour protéger vos données.</p>
    </div>
</div>

<div class="text-center pad">
    <div class="col-lg-6 mx-auto">
        <h2 class="Colorh2 text-center">Rejoignez la révolution médicale pour nos forces armées</h2>
        <p class="lead mb-4 mt-5 text-center">Inscrivez vous dès maintenant pour bénéficier des avantages de MilMedCare et contribuer à renforcer notre soutien médical pour nos héros en uniforme.</p>
    </div>
</div>


<?php require_once('includes/footer.php'); ?>