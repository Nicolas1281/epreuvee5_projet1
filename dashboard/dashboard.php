<?php
require_once('../includes/header.php');
require_once('../includes/navbarDash.php');
?>

<div class="container-fluid">
    <img class="img-fluid pad image-center" src="../assets/img/image5.jpg" alt="vehicule">

</div>

<div class="text-center pad border-down border-marge pb-5">
    <h1 class="display-5 fw-bold Colorh1">Bienvenue !</h1>
    <div class="col-lg-6 mx-auto">
        <h2 class="lead mb-4 Colorh2">Trouvez un rendez vous</h2>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-4">
                <select id="centerSelect" class="form-select" aria-label="Base d’affectation ou hôpital militaire">
                    <option selected>Base d’affectation ou hôpital militaire</option>
                </select>
            </div>
            <div class="col-md-4">
                <select id="specialitySelect" class="form-select" aria-label="Spécialité">
                    <option selected>Spécialité</option>
                </select>
            </div>
            <div class="col-md-1">
                <button class="btn-color" type="submit" onclick="rechercher()">Rechercher</button>
            </div>
        </div>
    </div>
</div>

<div class="text-center pad py-5">
    <div class="col-lg-6 mx-auto">
        <h2 class="lead mb-4 Colorh2">Prochains Rendez-vous</h2>
    </div>

    <div class="container">
        <div class="row">
            <div class="d-flex" id="reservationContainer"></div> <!-- Conteneur pour les réservations -->
        </div>
    </div>

    <div class="col-6 mx-auto mt-3">
        <button class="btn-color" type="button" id="voirPlusBtn">Voir plus de rendez-vous ...</button>
    </div>
</div>

<div class="text-start border-up border-marge pad">
    <div class="col-lg-6 mx-auto g-3 py-5">
        <h2 class="Colorh2 text-start mb-3">A propos de MilMedCare</h2>
        <p class="lead mb-4 colorp text-start">MilMedcare est une plateforme de gestion des rendez-vous médicaux conçue pour répondre aux besoins des forces armées.
            Notre objectif principal est de simplifier la prise de rendez-vous entre les patients militaires et les médecins militaires, tout en garantissant la
            confidentialité des données médicales. Notre engagement envers nos militaires est au cœur de notre mission.
        </p>
        <h2 class="Colorh2 text-start mb-3">Statistiques</h2>
        <p class="lead mb-4 colorp text-start">Chaque mois, MilMedcare enregistre plus de 10 000 rendez-vous médicaux, témoignant de la confiance que nos utilisateurs
            accordent à notre plateforme.
            <br>
            <br>
            Nous sommes fiers d'annoncer un taux de satisfaction des patients de 95 %, démontrant notre engagement à fournir des services médicaux de qualité qui
            répondent aux besoins de nos militaires.
            <br>
            <br>
            De plus, notre temps moyen d'attente pour un rendez-vous est inférieur à 2 jours, ce qui permet une prise en charge médicale rapide et efficace. Chez
            MilMedcare, la satisfaction de nos patients reste notre priorité.
        </p>
    </div>
</div>

<script>
    // Fonction pour rediriger vers la page de réservation
    function redirectToReservationPage() {
        window.location.href = "../views/Reservation.php";
    }

    // Ajouter un écouteur d'événement au bouton "Voir plus de rendez-vous"
    document.getElementById("voirPlusBtn").addEventListener("click", redirectToReservationPage);
</script>

<script src="../API/search_doctors/JS/center&speciality.js"></script>
<script src="../API/search_doctors/JS/search.js"></script>
<script src="../API/reservation/JS/read.js"></script>

<?php require_once('../includes/footerDash.php'); ?>