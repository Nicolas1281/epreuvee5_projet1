<body>

    <?php
    // Récupérer le nom de la page actuelle
    $current_page = basename($_SERVER['PHP_SELF']);
    ?>

    <!-- Barre de navigation -->
    <header class="d-flex flex-wrap align-items-center justify-content-between py-3 mb-4 border-down pad">
        <div>
            <a href="../index.php" class="d-inline-flex link-body-emphasis text-decoration-none">
                <img src="../assets/img/logo.png" alt="Logo MilMedCare" width="120">
            </a>
        </div>

        <div class="nav justify-content-center">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a href="../dashboard/dashboard.php" class="nav-link <?php if ($current_page == 'dashboard.php') echo 'active'; ?>">Accueil</a>
                </li>
                <li class="nav-item ms-2">
                    <a href="../views/Reservation.php" class="nav-link <?php if ($current_page == 'Reservation.php') echo 'active'; ?>">Mes réservations</a>
                </li>
                <li class="nav-item ms-2">
                    <a href="../views/medicalfolder.php" class="nav-link <?php if ($current_page == 'medicalfolder.php') echo 'active'; ?>">Mon dossier médical</a>
                </li>
            </ul>
        </div>

        <div class="text-end">
            <ul class="nav nav-pills">
                <li class="nav-item ms-2">
                    <a class="nav-link btn btn-outline-secondary <?php if ($current_page == 'Profil.php') echo 'active'; ?>" href="../views/Profil.php">
                        <i class="fas fa-sign-out-alt"><img src="/assets/img/icons8-utilisateur-24.png" alt=""></i>
                    </a>
                </li>
                <li class="nav-item ms-2">
                    <a class="nav-link btn btn-outline-secondary" href="../auth/logout.php">Déconnexion</a>
                </li>
            </ul>
        </div>
    </header>