<body>

    <?php
    // Récupérer le nom de la page actuelle
    $current_page = basename($_SERVER['PHP_SELF']);
    ?>

    <header class="d-flex flex-wrap align-items-center justify-content-between py-3 mb-4 border-down pad">
        <div>
            <a href="../index.php" class="d-inline-flex link-body-emphasis text-decoration-none">
                <img src="../assets/img/logo.png" alt="Logo MilMedCare" width="120">
            </a>
        </div>

        <div class="text-end">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a class="nav-link btn btn-outline-success mb-2 <?php if ($current_page == 'connexion.php') echo 'active'; ?>" href="../user/connexion.php">Connexion</a>
                </li>
                <li class="nav-item ms-2">
                    <a class="nav-link btn btn-outline-secondary <?php if ($current_page == 'inscription.php') echo 'active'; ?>" href="../user/inscription.php">S'inscrire</a>
                </li>
            </ul>
        </div>
    </header>