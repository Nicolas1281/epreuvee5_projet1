<?php

require_once('../includes/header.php');
require_once('../includes/navbar.php');
?>

<div class="text-center">
    <div class="col-lg-6 mx-auto g-3 py-3">
        <h1 class="Colorh1 text-center">Bienvenue sur MilMedCare</h1>
        <p class="lead mb-4 colorp text-center">Veuillez vous connecter à votre compte pour accéder à votre espace personnel</p>
    </div>
</div>

<div class="form-container mb-4">
    <h2 class="title">Connexion</h2>
    <form id="loginForm" method="POST" action="http://milmedcare/API/account/connexion.php?action=login" enctype="multipart/form-data">
        <div class="input-group">
            <label for="email">Adresse mail</label>
            <input type="email" name="email" id="email" placeholder="adresse.mail@email.com" value="">
            <span id="emailError" class="error"></span> <!-- Balise pour afficher l'erreur email -->
        </div>
        <div class="input-group">
            <label for="password">Mot de passe</label>
            <input type="password" name="password" id="password" placeholder="">
            <span id="passwordError" class="error"></span> <!-- Balise pour afficher l'erreur mot de passe -->
        </div>
        <p id="generalError" class="error"></p> <!-- Balise pour afficher les erreurs générales -->
        <button class="sign my-3" type="submit">Se connecter</button>
    </form>

    <p class="signup mt-3">Vous n'êtes pas inscrit?
        <a rel="noopener noreferrer" href="../user/inscription.php">Inscription</a>
    </p>
</div>



<div class="text-start">
    <div class="col-lg-6 mx-auto g-3 py-3">
        <p class="lead mb-4 colorp text-start">En vous connectant, vous acceptez nos conditions d'utilisation et notre politique de confidentialité.</p>
        <h2 class="Colorh2 text-start mb-3">A propos de MilMedCare</h2>
        <p class="lead mb-4 colorp text-start">MilMedcare est une plateforme de gestion des rendez-vous médicaux conçue pour répondre aux besoins des forces armées.
            Notre objectif principal est de simplifier la prise de rendez-vous entre les patients militaires et les médecins militaires, tout en garantissant la
            confidentialité des données médicales. Notre engagement envers nos militaires est au cœur de notre mission.
        </p>
        <h2 class="Colorh2 text-start mb-3">Statistiques</h2>
        <p class="lead mb-4 colorp text-start">Chaque mois, MilMedcare enregistre plus de 10 000 rendez-vous médicaux, témoignant de la confiance que nos utilisateurs
            accordent à notre plateforme.
            <br>
            <br>
            Nous sommes fiers d'annoncer un taux de satisfaction des patients de 95 %, démontrant notre engagement à fournir des services médicaux de qualité qui
            répondent aux besoins de nos militaires.
            <br>
            <br>
            De plus, notre temps moyen d'attente pour un rendez-vous est inférieur à 2 jours, ce qui permet une prise en charge médicale rapide et efficace. Chez
            MilMedcare, la satisfaction de nos patients reste notre priorité.
        </p>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        const loginForm = document.getElementById('loginForm');
        const form = document.querySelector('form');

        loginForm.addEventListener('submit', function(event) {
            event.preventDefault(); // Empêche la soumission par défaut du formulaire

            // Récupération des valeurs des champs email et password
            const email = document.getElementById('email').value;
            const password = document.getElementById('password').value;

            // Réinitialisation des messages d'erreur
            document.getElementById('emailError').textContent = '';
            document.getElementById('passwordError').textContent = '';

            // Validation des champs email et password
            if (!email) {
                document.getElementById('emailError').textContent = 'Veuillez saisir votre adresse e-mail.';
                return; // Arrête l'exécution de la fonction si le champ email est vide
            }
            if (!password) {
                document.getElementById('passwordError').textContent = 'Veuillez saisir votre mot de passe.';
                return; // Arrête l'exécution de la fonction si le champ password est vide
            }

            // Envoi du formulaire via Fetch
            const formData = new FormData(form);
            fetch(form.action, {
                    method: form.method,
                    body: formData,
                })
                .then(response => response.json())
                .then(data => {
                    if (data.success) {
                        // Redirect to dashboard.php on successful login
                        window.location.href = '../../dashboard/dashboard.php';
                    } else {
                        // Handle unsuccessful login (display an error message, etc.)
                        console.error(data.message);
                    }
                })
                .catch(error => console.error('Error:', error));
        });
    });
</script>


<?php require_once('../includes/footer.php'); ?>