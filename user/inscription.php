<?php
require_once('../includes/header.php');
require_once('../includes/navbar.php');
?>

<div class="text-center">
    <div class="col-lg-6 mx-auto g-3 py-3">
        <h1 class="Colorh1 text-center">Bienvenue sur MilMedCare</h1>
        <p class="lead mb-4 colorp text-center">Rejoignez MilMedCare et bénéficiez d’un accés personnalisé à vos rendez-vous médicaux</p>
    </div>
</div>

<div class="form-container my-2">
    <p class="title">Inscription</p>
    <form class="form" method="POST" action="http://milmedcare/API/account/create.php?action=create" enctype="multipart/form-data">
        <div class="input-group">
            <label for="username">Nom</label>
            <input type="text" name="username" id="username" placeholder="" value="">
        </div>
        <div class="input-group">
            <label for="firstname">Prenom</label>
            <input type="text" name="firstname" id="firstname" placeholder="" value="">
        </div>
        <div class="input-group">
            <label for="address">Code postal</label>
            <input type="text" name="codepostal" id="codepostal" placeholder="" value="">
        </div>
        <div class="input-group">
            <label for="address">Adresse</label>
            <input type="text" name="address" id="address" placeholder="" value="">
        </div>
        <div class="input-group">
            <label for="choix">Lieux d'affectation</label>
            <select name="choix" id="choix">
                <option value="base-aerienne">Base Aérienne</option>
                <option value="caserne">Caserne</option>
                <option value="base-navale">Base Navale</option>
            </select>
        </div>
        <div class="input-group">
            <label for="email">Adresse mail</label>
            <input type="email" name="email" id="email" placeholder="adresse.mail@email.com" value="">
        </div>
        <div class="input-group">
            <label for="phone">téléphone</label>
            <input type="phone" name="phone" id="phone" placeholder="" value="">
        </div>
        <div class="input-group">
            <label for="birthdate">Date de naissance</label>
            <input type="date" name="birthdate" id="birthdate" placeholder="" value="">
        </div>
        <div class="input-group">
            <label for="password">Mot de passe</label>
            <input type="password" name="password" id="password" placeholder="1 caractère spécial (!@#$%^&*()_-+={};:,<.>), 1 majuscule, 1 minuscule et 1 chiffre.">
        </div>
        <div class="input-group">
            <label for="confirm-password">Confirmer Mot de passe</label>
            <input type="password" name="confirm-password" id="confirm-password" placeholder="1 caractère spécial (!@#$%^&*()_-+={};:,<.>), 1 majuscule, 1 minuscule et 1 chiffre.">
        </div>
        <div id="errorMessage" class="mt-3" style="color: red;"></div> <!-- Message d'erreur général -->
        <div id="successMessage" style="color: green;"></div>
        <br>
        <button class="sign">S'inscrire</button>
    </form>
    <p class="signup mt-3">Vous avez déjà un compte?
        <a rel="noopener noreferrer" href="../user/connexion.php" class="">Connexion</a>
    </p>
</div>

<div class="text-start">
    <div class="col-lg-6 mx-auto g-3 py-3">
        <p class="lead mb-4 colorp text-start">En vous inscrivant, vous acceptez nos conditions d'utilisation et notre politique de confidentialité.</p>
        <h2 class="Colorh2 text-start mb-3">A propos de MilMedCare</h2>
        <p class="lead mb-4 colorp text-start">MilMedcare est une plateforme de gestion des rendez-vous médicaux conçue pour répondre aux besoins des forces armées.
            Notre objectif principal est de simplifier la prise de rendez-vous entre les patients militaires et les médecins militaires, tout en garantissant la
            confidentialité des données médicales. Notre engagement envers nos militaires est au cœur de notre mission.
        </p>
        <h2 class="Colorh2 text-start mb-3">Statistiques</h2>
        <p class="lead mb-4 colorp text-start">Chaque mois, MilMedcare enregistre plus de 10 000 rendez-vous médicaux, témoignant de la confiance que nos utilisateurs
            accordent à notre plateforme.
            <br>
            <br>
            Nous sommes fiers d'annoncer un taux de satisfaction des patients de 95 %, démontrant notre engagement à fournir des services médicaux de qualité qui
            répondent aux besoins de nos militaires.
            <br>
            <br>
            De plus, notre temps moyen d'attente pour un rendez-vous est inférieur à 2 jours, ce qui permet une prise en charge médicale rapide et efficace. Chez
            MilMedcare, la satisfaction de nos patients reste notre priorité.
        </p>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        const form = document.querySelector('.form');
        const errorMessage = document.getElementById('errorMessage');
        const successMessage = document.getElementById('successMessage');

        form.addEventListener('submit', function(event) {
            event.preventDefault(); // Empêche la soumission du formulaire par défaut

            // Récupération des valeurs des champs
            const username = document.getElementById('username').value;
            const firstname = document.getElementById('firstname').value;
            const codepostal = document.getElementById('codepostal').value;
            const address = document.getElementById('address').value;
            const choix = document.getElementById('choix').value;
            const email = document.getElementById('email').value;
            const phone = document.getElementById('phone').value;
            const birthdate = document.getElementById('birthdate').value;
            const password = document.getElementById('password').value;
            const confirmPassword = document.getElementById('confirm-password').value;

            // Réinitialisation des messages d'erreur
            errorMessage.textContent = '';
            successMessage.textContent = ''; // Réinitialise le message de succès

            // Validation des champs requis
            if (!username || !firstname || !codepostal || !address || !choix || !email || !phone || !birthdate || !password || !confirmPassword) {
                errorMessage.textContent = 'Veuillez remplir tous les champs.';
                return;
            }

            // Validation des mots de passe
            if (password !== confirmPassword) {
                errorMessage.textContent = 'Les mots de passe ne correspondent pas.';
                return;
            }

            const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*()_\-+={};:,<.>]).{8,}$/;
            if (!passwordRegex.test(password)) {
                errorMessage.textContent = 'Le mot de passe doit contenir au moins 8 caractères, avec au moins une lettre minuscule, une lettre majuscule, un chiffre et un caractère spécial.';
                return;
            }

            // Envoi du formulaire via Fetch
            const formData = new FormData(form);
            fetch(form.action, {
                    method: form.method,
                    body: formData,
                })
                .then(response => response.json())
                .then(data => {
                    if (data.success) {
                        // Si l'inscription est réussie, rediriger vers la page de connexion
                        window.location.href = '../user/connexion.php';
                    } else {
                        // Si une erreur se produit, affichez le message d'erreur dans la console.
                        console.error(data.message);
                    }
                })
                .catch(error => console.error('Erreur:', error));
        });
    });
</script>

<?php require_once('../includes/footer.php'); ?>