<?php
/*session_start();*/

// Vérifier si l'utilisateur est connecté
if (isset($_SESSION['email'])) {
    // Si l'utilisateur est connecté et essaie d'accéder à index.php, redirigez vers dashboard.php
    if (basename($_SERVER['PHP_SELF']) === 'index.php') {
        header('Location: ../dashboard/dashboard.php');
        exit;
    }
} else {
    // Si l'utilisateur n'est pas connecté et essaie d'accéder à dashboard.php, redirigez vers index.php
    if (basename($_SERVER['PHP_SELF']) === 'dashboard.php') {
        header('Location: ../index.php');
        exit;
    }
}
