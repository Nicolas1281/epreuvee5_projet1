<?php
require_once("../db_connect.php");
require_once('../config.php');

// Lire les examens médicaux
if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET['action']) && $_GET['action'] === 'read_examens') {
    if (isset($_GET['id'])) {
        // Utiliser l'ID fourni dans la requête
        $userID = $_GET['id'];
        $examens = getExamens($conn, $userID);

        if ($examens) {
            echo json_encode($examens);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucun examen trouvé"]);
        }
    } elseif (isset($_SESSION['userID'])) {
        // Utiliser l'ID stocké dans la session si l'utilisateur est connecté
        $userID = $_SESSION['userID'];
        $examens = getExamens($conn, $userID);

        if ($examens) {
            echo json_encode($examens);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucun examen trouvé"]);
        }
    } else {
        http_response_code(401);
        echo json_encode(["message" => "Non autorisé. Veuillez vous connecter."]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}

// Fonction pour récupérer les examens médicaux
function getExamens($conn, $userID)
{
    // Construisez la requête SQL pour récupérer les examens médicaux associés à cet utilisateur
    $query = "SELECT * FROM examens WHERE account_id = $userID";

    $result = $conn->query($query);

    if ($result) {
        $examens = array(); // Créez un tableau pour stocker les examens

        while ($row = $result->fetch_assoc()) {
            // Ajoutez chaque examen à votre tableau
            $examens[] = array(
                "id" => $row['id'],
                "Date" => $row['Date'],
                "Type_examen" => $row['Type_examen'],
                "Resultats" => $row['Resultats'],
                "Remarques" => $row['Remarques'],
            );
        }

        return $examens;
    } else {
        return null;
    }
}
