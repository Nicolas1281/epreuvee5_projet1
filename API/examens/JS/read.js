/* =========================
Gestion récuération des examens
========================= */

// Fonction pour créer un conteneur pour chaque examens
function createContainerExamens(examens) {
  // Créer le conteneur principal
  var container = document.createElement("div");
  container.classList.add("mt-5");

  // Ajouter le titre
  var title = document.createElement("h3");
  title.classList.add("card-title", "mt-5", "Colorh3");
  title.textContent = `Examens ${examens.id}`;
  container.appendChild(title);

  // Ajouter le paragraphe pour la Date de l'examens
  var typeParagraph = document.createElement("p");
  typeParagraph.classList.add("card-text", "mt-3");
  typeParagraph.innerHTML = `<strong>Date de l’examen:</strong> ${examens.Date}`;
  container.appendChild(typeParagraph);

  // Ajouter le paragraphe pour le Type de l’examen
  var descriptionParagraph = document.createElement("p");
  descriptionParagraph.classList.add("card-text");
  descriptionParagraph.innerHTML = `<strong>Type de l’examen:</strong> ${examens.Type_examen}`;
  container.appendChild(descriptionParagraph);

  // Ajouter le paragraphe pour les Résultats de l’examen
  var typeParagraph = document.createElement("p");
  typeParagraph.classList.add("card-text", "mt-3");
  typeParagraph.innerHTML = `<strong>Résultats de l’examen:</strong> ${examens.Resultats}`;
  container.appendChild(typeParagraph);

  // Ajouter le paragraphe pour les Remarques
  var descriptionParagraph = document.createElement("p");
  descriptionParagraph.classList.add("card-text");
  descriptionParagraph.innerHTML = `<strong>Remarques:</strong> ${examens.Remarques}`;
  container.appendChild(descriptionParagraph);

  // Ajouter le séparateur
  var separator = document.createElement("hr");
  container.appendChild(separator);

  // Ajouter le conteneur principal à l'élément ayant l'ID "examensFormContainer"
  document.getElementById("examensFormContainer").appendChild(container);
}

// Appeler l'API pour récupérer les diagnostics
fetch("http://milmedcare/API/examens/read.php?action=read_examens")
  .then((response) => {
    if (!response.ok) {
      throw new Error(`Erreur HTTP! Statut: ${response.status}`);
    }
    return response.text(); // Retourne le texte brut de la réponse
  })
  .then((data) => {
    return JSON.parse(data); // Essayez de parser la réponse en JSON
  })
  .then((parsedData) => {
    if (parsedData.length > 0) {
      parsedData.forEach((examens) => createContainerExamens(examens));
    } else {
      document.getElementById("examensFormContainer").innerHTML =
        "Aucun examens trouvé.";
    }
  })
  .catch((error) =>
    console.error("Erreur lors de la récupération des examens:", error)
  );
