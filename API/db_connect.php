<?php
$server = "localhost";
$username = "root";
$password = "";
$db = "milmedcare";

// Établir une connexion à la base de données
$conn = new mysqli($server, $username, $password, $db);

// Vérifier la connexion
if ($conn->connect_error) {
    die("La connexion à la base de données a échoué : " . $conn->connect_error);
}
