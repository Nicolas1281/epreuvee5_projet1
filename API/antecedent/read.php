<?php
require_once("../db_connect.php");
require_once('../config.php');

// Lire les antécédents médicaux
if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET['action']) && $_GET['action'] === 'read_antecedent') {
    if (isset($_GET['id'])) {
        // Utiliser l'ID fourni dans la requête
        $userID = $_GET['id'];
        $antecedents = getAntecedents($conn, $userID);

        if ($antecedents) {
            echo json_encode($antecedents);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucun antécédent trouvé"]);
        }
    } elseif (isset($_SESSION['userID'])) {
        // Utiliser l'ID stocké dans la session si l'utilisateur est connecté
        $userID = $_SESSION['userID'];
        $antecedents = getAntecedents($conn, $userID);

        if ($antecedents) {
            echo json_encode($antecedents);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucun antécédent trouvé"]);
        }
    } else {
        http_response_code(401);
        echo json_encode(["message" => "Non autorisé. Veuillez vous connecter."]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}

// Fonction pour récupérer les antécédents médicaux
function getAntecedents($conn, $userID)
{
    // Construisez la requête SQL pour récupérer les antécédents médicaux associés à cet utilisateur
    $query = "SELECT * FROM antecedent WHERE account_id = $userID";

    $result = $conn->query($query);

    if ($result) {
        $antecedents = array(); // Créez un tableau pour stocker les antécédents

        while ($row = $result->fetch_assoc()) {
            // Ajoutez chaque antécédent à votre tableau
            $antecedents[] = array(
                "antecedent_id" => $row['antecedent_id'],
                "type_antecedent" => $row['type_antecedent'],
                "description_antecedent" => $row['description_antecedent'],
            );
        }

        return $antecedents;
    } else {
        return null;
    }
}
