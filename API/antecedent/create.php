<?php
require_once("../db_connect.php");
require_once('../config.php');

// Créer un antécédent
if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['action']) && $_POST['action'] === 'create_antecedent') {
    // Vérifier si l'utilisateur est connecté
    if (isset($_SESSION['userID'])) {
        // Récupérer l'ID du compte à partir de la session
        $account_id = $_SESSION['userID'];

        // Récupérer les données du formulaire
        $type_antecedent = isset($_POST['antecedentType']) ? $_POST['antecedentType'] : '';
        $description_antecedent = isset($_POST['antecedentDescription']) ? $_POST['antecedentDescription'] : '';

        // Validate data
        if (empty($type_antecedent) || empty($description_antecedent)) {
            http_response_code(400);
            echo json_encode(["error" => "AntecedentType and AntecedentDescription are required."]);
            exit();
        }

        // Requête SQL avec une instruction préparée pour insérer des données dans la base de données
        $sql = "INSERT INTO antecedent (account_id, type_antecedent, description_antecedent, created_at, modified_at) VALUES (?, ?, ?, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP)";

        // Créer une instruction préparée
        $stmt = $conn->prepare($sql);

        // Vérifier la préparation de l'instruction
        if (!$stmt) {
            http_response_code(500);
            echo json_encode(["error" => "Error in preparing statement: " . $conn->error]);
            exit();
        } else {
            // Debugging: Log the SQL query
            error_log("SQL Query: " . $sql);
        }

        // Lier les paramètres à l'instruction préparée
        $stmt->bind_param("iss", $account_id, $type_antecedent, $description_antecedent);

        // Exécuter l'instruction préparée
        if ($stmt->execute()) {
            echo json_encode(["message" => "Antécédent créé avec succès."]);
        } else {
            http_response_code(500);
            echo json_encode(["error" => "Erreur lors de la création de l'antécédent: " . $stmt->error]);
        }

        // Fermer l'instruction préparée
        $stmt->close();
    } else {
        http_response_code(401);
        echo json_encode(["message" => "Non autorisé. Veuillez vous connecter."]);
    }

    // Fermer la connexion à la base de données
    $conn->close();
} else {
    http_response_code(400);
    echo json_encode(["message" => "Mauvaise requête. Action manquante ou invalide."]);
}
