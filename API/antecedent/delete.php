<?php
require_once("../db_connect.php");
require_once('../config.php');


if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['action']) && $_POST['action'] === 'delete_antecedent') {
    // Vérifiez si un ID d'antécédent a été fourni dans la requête
    if (isset($_POST['antecedent_id'])) {
        $antecedentID = $_POST['antecedent_id'];

        // Construisez la requête SQL pour supprimer l'antécédent de la base de données
        $query = "DELETE FROM antecedent WHERE antecedent_id = $antecedentID";

        $result = $conn->query($query);

        if ($result) {
            http_response_code(200);
            echo json_encode(["message" => "Antécédent supprimé avec succès"]);
        } else {
            http_response_code(500);
            echo json_encode(["message" => "Erreur lors de l'exécution de la requête : " . $conn->error]);
        }
    } else {
        http_response_code(400);
        echo json_encode(["message" => "Paramètre 'antecedent_id' manquant"]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}
?>
