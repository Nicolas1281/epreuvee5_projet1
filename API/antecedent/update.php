<?php
require_once("../db_connect.php");
require_once('../config.php');


if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['action']) && $_POST['action'] === 'update_antecedent') {
    // Vérifiez si tous les paramètres nécessaires ont été fournis
    if (isset($_POST['antecedent_id']) && isset($_POST['type_antecedent']) && isset($_POST['description_antecedent'])) {
        $antecedentID = $_POST['antecedent_id'];
        $newType = $_POST['type_antecedent'];
        $newDescription = $_POST['description_antecedent'];

        // Construisez la requête SQL pour mettre à jour l'antécédent dans la base de données
        $query = "UPDATE antecedent SET type_antecedent = '$newType', description_antecedent = '$newDescription' WHERE antecedent_id = $antecedentID";

        $result = $conn->query($query);

        if ($result) {
            http_response_code(200);
            echo json_encode(["message" => "Antécédent mis à jour avec succès"]);
        } else {
            http_response_code(500);
            echo json_encode(["message" => "Erreur lors de l'exécution de la requête : " . $conn->error]);
        }
    } else {
        http_response_code(400);
        echo json_encode(["message" => "Paramètres manquants"]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}
