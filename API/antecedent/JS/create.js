/* =============================================
Appeler l'API pour créer un antécédent médicaux
================================================ */

$(document).ready(function () {
  $("#addAntecedentBtn").on("click", function () {
    $(this).hide();
    $("#antecedentForm").show();
    $("#saveAntecedentBtn, #cancelAntecedentBtn").show();
  });

  $("#createAntecedentForm").submit(function (e) {
    e.preventDefault();
    var antecedentType = $("#antecedentType").val();
    var antecedentDescription = $("#antecedentDescription").val();

    $.ajax({
      type: "POST",
      url: "http://milmedcare/API/antecedent/create.php",
      data: {
        action: "create_antecedent",
        antecedentType: antecedentType,
        antecedentDescription: antecedentDescription,
      },
      success: function (response) {
        console.log(response);
        $("#createAntecedentForm")[0].reset();
        $("#antecedentForm, #saveAntecedentBtn, #cancelAntecedentBtn").hide();
        $("#addAntecedentBtn").show();
        // Rafraîchir la page après le succès
        location.reload();
      },
      error: function (error) {
        console.log(error);
      },
    });
  });

  $("#cancelAntecedentBtn").on("click", function () {
    $("#createAntecedentForm")[0].reset();
    $("#antecedentForm, #saveAntecedentBtn, #cancelAntecedentBtn").hide();
    $("#addAntecedentBtn").show();
  });
});
