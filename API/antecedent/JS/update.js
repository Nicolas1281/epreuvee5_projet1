/* =================================================
Fonction pour afficher le formulaire de modification
==================================================== */

function editAntecedent(antecedent) {
  // Récupérer le conteneur de l'antécédent à mettre à jour
  var containerToUpdate = event.target.closest(".mt-5");

  // Créer le formulaire de mise à jour
  var updateForm = document.createElement("form");
  updateForm.classList.add("mt-3");

  // Champ de type d'antécédent
  var typeInput = document.createElement("input");
  typeInput.type = "text";
  typeInput.value = antecedent.type_antecedent;
  typeInput.classList.add("form-control", "mb-2");
  updateForm.appendChild(typeInput);

  // Champ de description d'antécédent
  var descriptionInput = document.createElement("input");
  descriptionInput.type = "text";
  descriptionInput.value = antecedent.description_antecedent;
  descriptionInput.classList.add("form-control", "mb-2");
  updateForm.appendChild(descriptionInput);

  // Bouton Enregistrer
  var saveButton = document.createElement("button");
  saveButton.type = "button"; // Éviter la soumission du formulaire
  saveButton.textContent = "Enregistrer";
  saveButton.classList.add("btn", "btn-success", "mb-2");
  // Ajouter un gestionnaire d'événements pour le bouton Enregistrer
  saveButton.addEventListener("click", function () {
    // Appeler la fonction de mise à jour avec les nouvelles valeurs
    updateAntecedent(
      antecedent.antecedent_id,
      typeInput.value,
      descriptionInput.value,
      containerToUpdate
    );
  });
  updateForm.appendChild(saveButton);

  // Bouton Annuler
  var cancelButton = document.createElement("button");
  cancelButton.type = "button";
  cancelButton.textContent = "Annuler";
  cancelButton.classList.add("btn", "btn-secondary", "ms-2", "mb-2");
  // Ajouter un gestionnaire d'événements pour le bouton Annuler
  cancelButton.addEventListener("click", function () {
    // Remettre en place le contenu initial lorsque l'utilisateur annule
    resetContainer(containerToUpdate, antecedent);
  });
  updateForm.appendChild(cancelButton);

  // Remplacer le contenu du conteneur par le formulaire de mise à jour
  containerToUpdate.innerHTML = "";
  containerToUpdate.appendChild(updateForm);
}

// Fonction pour réinitialiser le contenu du conteneur à l'état initial
function resetContainer(container, antecedent) {
  container.innerHTML = ""; // Effacer le contenu du conteneur

  // Ajouter le titre
  var title = document.createElement("h3");
  title.classList.add("card-title", "mt-5", "Colorh3");
  title.textContent = `Antécédent ${antecedent.antecedent_id}`;
  container.appendChild(title);

  // Ajouter le paragraphe pour le type d'antécédent
  var typeParagraph = document.createElement("p");
  typeParagraph.classList.add("card-text", "mt-3");
  typeParagraph.innerHTML = `<strong>Type d’antécédent:</strong> ${antecedent.type_antecedent}`;
  container.appendChild(typeParagraph);

  // Ajouter le paragraphe pour la description de l'antécédent
  var descriptionParagraph = document.createElement("p");
  descriptionParagraph.classList.add("card-text");
  descriptionParagraph.innerHTML = `<strong>Description de l’antécédent:</strong> ${antecedent.description_antecedent}`;
  container.appendChild(descriptionParagraph);

  // Ajouter le bouton Modifier
  var editButton = document.createElement("button");
  editButton.textContent = "Modifier";
  editButton.classList.add("btn", "btn-primary", "mt-3");
  // Ajouter un gestionnaire d'événements pour le bouton Modifier
  editButton.addEventListener("click", function () {
    // Appeler une fonction d'édition avec l'ID de l'antécédent
    editAntecedent(antecedent);
  });
  container.appendChild(editButton);

  // Ajouter le bouton Supprimer
  var deleteButton = document.createElement("button");
  deleteButton.textContent = "Supprimer";
  deleteButton.classList.add("btn", "btn-danger", "mt-3", "ms-2");
  // Ajouter un gestionnaire d'événements pour le bouton Supprimer
  deleteButton.addEventListener("click", function () {
    // Appeler une fonction de suppression avec l'ID de l'antécédent
    deleteAntecedent(antecedent.antecedent_id);
  });
  container.appendChild(deleteButton);

  // Ajouter le séparateur
  var separator = document.createElement("hr");
  container.appendChild(separator);
}

/* ================================================
  Fonction pour mettre à jour l'antécédent via l'API
  =================================================== */

function updateAntecedent(
  antecedentID,
  newType,
  newDescription,
  containerToUpdate
) {
  // Créer un objet avec les données à envoyer à l'API
  var updateData = {
    action: "update_antecedent",
    antecedent_id: antecedentID,
    type_antecedent: newType,
    description_antecedent: newDescription,
  };

  // Envoyer la requête de mise à jour à l'API
  fetch("http://milmedcare/API/antecedent/update.php", {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded", // Assurez-vous d'ajuster le type de contenu en fonction de ce que votre API attend
    },
    body: new URLSearchParams(updateData),
  })
    .then((response) => response.json())
    .then((data) => {
      // Traiter la réponse de l'API ici
      console.log(data);

      // Mettez à jour l'interface utilisateur ou effectuez d'autres actions nécessaires après la modification
      resetContainer(containerToUpdate, updateData);
    })
    .catch((error) => {
      console.error("Erreur lors de la mise à jour de l'antécédent:", error);

      // Gérer les erreurs ici (afficher un message d'erreur, par exemple)
      alert(error.message || "Erreur lors de la mise à jour de l'antécédent.");
    });
}
