/* ======================================================
Fonction pour supprimer un antécédent avec confirmation
======================================================== */

function deleteAntecedent(antecedentId) {
  // Demander confirmation à l'utilisateur
  var isConfirmed = window.confirm(
    "Êtes-vous sûr de vouloir supprimer cet antécédent ?"
  );

  // Si l'utilisateur confirme, appeler l'API de suppression
  if (isConfirmed) {
    fetch("http://milmedcare/API/antecedent/delete.php", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: `action=delete_antecedent&antecedent_id=${antecedentId}`,
    })
      .then((response) => response.json())
      .then((data) => {
        console.log(data.message);
        // Vous pouvez également mettre à jour l'interface utilisateur en supprimant le conteneur de l'antécédent supprimé
        var containerToDelete = document.querySelector(
          `div[data-antecedent-id="${antecedentId}"]`
        );
        if (containerToDelete) {
          containerToDelete.remove();
        }

        // Actualiser la page
        window.location.reload();
      })
      .catch((error) =>
        console.error("Erreur lors de la suppression de l'antécédent:", error)
      );
  }
}
