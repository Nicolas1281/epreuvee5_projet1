/* ==========================================================
Gestion récuération des antecedents sur le profil du patient
============================================================= */

// Fonction pour créer un conteneur pour chaque antécédent
function createContainerAntecedent(antecedent) {
  // Créer le conteneur principal
  var container = document.createElement("div");
  container.classList.add("mt-5");

  // Ajouter le titre
  var title = document.createElement("h3");
  title.classList.add("card-title", "mt-5", "Colorh3");
  title.textContent = `Antécédent ${antecedent.antecedent_id}`;
  container.appendChild(title);

  // Ajouter le paragraphe pour le type d'antécédent
  var typeParagraph = document.createElement("p");
  typeParagraph.classList.add("card-text", "mt-3");
  typeParagraph.innerHTML = `<strong>Type d’antécédent:</strong> ${antecedent.type_antecedent}`;
  container.appendChild(typeParagraph);

  // Ajouter le paragraphe pour la description de l'antécédent
  var descriptionParagraph = document.createElement("p");
  descriptionParagraph.classList.add("card-text");
  descriptionParagraph.innerHTML = `<strong>Description de l’antécédent:</strong> ${antecedent.description_antecedent}`;
  container.appendChild(descriptionParagraph);

  // Ajouter le bouton Modifier
  var editButton = document.createElement("button");
  editButton.textContent = "Modifier";
  editButton.classList.add("btn", "btn-success", "mt-3");
  // Ajouter un gestionnaire d'événements pour le bouton Modifier
  editButton.addEventListener("click", function () {
    // Appeler une fonction d'édition avec l'ID de l'antécédent
    editAntecedent(antecedent);
  });
  container.appendChild(editButton);

  // Ajouter le bouton Supprimer
  var deleteButton = document.createElement("button");
  deleteButton.textContent = "Supprimer";
  deleteButton.classList.add("btn", "btn-danger", "mt-3", "ms-2");
  // Ajouter un gestionnaire d'événements pour le bouton Supprimer
  deleteButton.addEventListener("click", function () {
    // Appeler une fonction de suppression avec l'ID de l'antécédent
    deleteAntecedent(antecedent.antecedent_id);
  });
  container.appendChild(deleteButton);

  // Ajouter le séparateur
  var separator = document.createElement("hr");
  container.appendChild(separator);

  // Ajouter le conteneur principal à l'élément ayant l'ID "antecedentFormContainer"
  document.getElementById("antecedentFormContainer").appendChild(container);
}

/* =======================================================
Appeler l'API pour récupérer les antécédents médicaux
======================================================= */

fetch("http://milmedcare/API/antecedent/read.php?action=read_antecedent")
  .then((response) => {
    if (!response.ok) {
      throw new Error(`Erreur HTTP! Statut: ${response.status}`);
    }
    return response.text(); // Retourne le texte brut de la réponse
  })
  .then((data) => {
    return JSON.parse(data); // Essayez de parser la réponse en JSON
  })
  .then((parsedData) => {
    if (parsedData.length > 0) {
      parsedData.forEach((antecedent) => createContainerAntecedent(antecedent));
    } else {
      document.getElementById("antecedentFormFolder").innerHTML =
        "Aucun antécédent trouvé.";
    }
  })
  .catch((error) =>
    console.error("Erreur lors de la récupération des antécédents:", error)
  );
