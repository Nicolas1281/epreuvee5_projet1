/* =========================
Suppression du compte
========================= */
// URL de l'API pour supprimer le compte
const deleteAccountApiUrl =
  "http://milmedcare/API/account/delete.php?action=delete";

document.getElementById("deleteAccountButton").addEventListener("click", () => {
  // Ouvrez le modal de confirmation
  let deleteConfirmationModal = new bootstrap.Modal(
    document.getElementById("deleteConfirmationModal")
  );
  deleteConfirmationModal.show();
});

document.getElementById("confirmDeleteButton").addEventListener("click", () => {
  // Ici, effectuez l'appel API réel pour supprimer le compte sans en-tête d'authentification
  fetch(deleteAccountApiUrl, {
    method: "DELETE",
  })
    .then((response) => {
      if (response.status === 204) {
        // Suppression réussie, redirigez vers index.php
        window.location.href = "../index.php";
      } else {
        alert("Erreur lors de la suppression du compte.");
      }
    })
    .catch((error) => {
      console.error(error);
    });

  // Fermez le modal
  let deleteConfirmationModal = new bootstrap.Modal(
    document.getElementById("deleteConfirmationModal")
  );
  deleteConfirmationModal.hide();
});
