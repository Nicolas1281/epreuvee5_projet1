/* =========================
Gestion de l'affichage des informations du profil et du formulaire pour modifier le profil
========================= */
document.addEventListener("DOMContentLoaded", function () {
  const editProfileBtn = document.getElementById("editProfileBtn");
  const profileInfo = document.getElementById("profileInfo");
  const profileForm = document.getElementById("profileForm");
  const profileUpdateForm = document.getElementById("profileUpdateForm");
  const newUserFullName = document.getElementById("newUserFullName");
  const newUserFirstName = document.getElementById("newUserFirstName");
  const newDateOfBirth = document.getElementById("newDateOfBirth");
  const newGender = document.getElementById("newGender");
  const newBloodGroup = document.getElementById("newBloodGroup");
  const newAllergies = document.getElementById("newAllergies");
  const newAddress = document.getElementById("newAddress");
  const newPostalCode = document.getElementById("newPostalCode");
  const newCity = document.getElementById("newCity");
  const newPhone = document.getElementById("newPhone");
  const newEmail = document.getElementById("newEmail");
  const newSocialSecurityNumber = document.getElementById(
    "newSocialSecurityNumber"
  );

  // Gestion du bouton "Modifier le profil"
  editProfileBtn.addEventListener("click", function () {
    // Si le formulaire est déjà affiché, masquez-le et affichez les informations du profil
    if (profileForm.style.display === "block") {
      profileForm.style.display = "none";
      profileInfo.style.display = "block";
    } else {
      // Sinon, masquez les informations du profil et affichez le formulaire
      profileInfo.style.display = "none";
      profileForm.style.display = "block";

      // Récupérez les informations actuelles du patient depuis l'API
      fetch("http://milmedcare/API/account/read.php?action=read")
        .then((response) => response.json())
        .then((data) => {
          // Pré-remplissez le formulaire avec les informations actuelles
          newUserFullName.value = data.userFullName;
          newUserFirstName.value = data.userFirstName;
          newDateOfBirth.value = data.dateOfBirth;
          newGender.value = data.gender;
          newBloodGroup.value = data.bloodGroup;
          newAllergies.value = data.allergies;
          newAddress.value = data.address;
          newPostalCode.value = data.postalCode;
          newCity.value = data.city;
          newPhone.value = data.phone;
          newEmail.value = data.email;
          newSocialSecurityNumber.value = data.socialSecurityNumber;
          // Pré-remplissez d'autres champs du formulaire ici
        })
        .catch((error) => {
          console.error(
            "Erreur lors de la récupération des informations du patient :",
            error
          );
        });
    }
  });

  // Gestion de la soumission du formulaire
  profileUpdateForm.addEventListener("submit", function (e) {
    e.preventDefault();

    // Récupérez les données du formulaire et envoyez-les à l'API pour la mise à jour du profil
    const updatedData = {
      userFullName: newUserFullName.value,
      userFirstName: newUserFirstName.value,
      dateOfBirth: newDateOfBirth.value,
      gender: newGender.value,
      bloodGroup: newBloodGroup.value,
      allergies: newAllergies.value,
      address: newAddress.value,
      postalCode: newPostalCode.value,
      city: newCity.value,
      phone: newPhone.value,
      email: newEmail.value,
      socialSecurityNumber: newSocialSecurityNumber.value,
      // Ajoutez d'autres données mises à jour ici
    };

    // Envoyez les données à l'API pour la mise à jour du profil
    fetch("http://milmedcare/API/account/update.php?action=update", {
      method: "PUT", // Utilisez la méthode PUT pour la mise à jour
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updatedData),
    })
      .then((response) => response.json())
      .catch((error) => {
        console.error("Erreur lors de la mise à jour du profil :", error);
      });

    // Masquez le formulaire et réaffichez les informations du profil
    profileForm.style.display = "none";
    profileInfo.style.display = "block";

    // Mettez à jour les informations du profil avec les nouvelles données
    document.getElementById("userFullName").textContent = newUserFullName.value;
    document.getElementById("userFirstName").textContent =
      newUserFirstName.value;
    document.getElementById("dateOfBirth").textContent = newDateOfBirth.value;
    document.getElementById("gender").textContent = newGender.value;
    document.getElementById("bloodGroup").textContent = newBloodGroup.value;
    document.getElementById("allergies").textContent = newAllergies.value;
    document.getElementById("address").textContent = newAddress.value;
    document.getElementById("postalCode").textContent = newPostalCode.value;
    document.getElementById("city").textContent = newCity.value;
    document.getElementById("phone").textContent = newPhone.value;
    document.getElementById("email").textContent = newEmail.value;
    document.getElementById("socialSecurityNumber").textContent =
      newSocialSecurityNumber.value;
  });
});
