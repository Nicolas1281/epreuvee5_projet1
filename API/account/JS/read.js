/* =========================
Gestion récuération des informations du patient
========================= */
document.addEventListener("DOMContentLoaded", function () {
  // Récupérer les informations du patient depuis l'API (par exemple, en utilisant fetch)
  fetch("http://milmedcare/API/account/read.php?action=read")
    .then((response) => response.json())
    .then((data) => {
      // Mettre à jour les éléments HTML avec les données du patient
      document.getElementById("userFullName").textContent = data.userFullName;
      document.getElementById("userFirstName").textContent = data.userFirstName;
      document.getElementById("userFirstName2").textContent =
        data.userFirstName;
      document.getElementById("dateOfBirth").textContent = data.dateOfBirth;
      document.getElementById("gender").textContent = data.gender;
      document.getElementById("bloodGroup").textContent = data.bloodGroup;
      document.getElementById("allergies").textContent = data.allergies;
      document.getElementById("address").textContent = data.address;
      document.getElementById("postalCode").textContent = data.postalCode;
      document.getElementById("city").textContent = data.city;
      document.getElementById("phone").textContent = data.phone;
      document.getElementById("email").textContent = data.email;
      document.getElementById("socialSecurityNumber").textContent =
        data.socialSecurityNumber;
    })
    .catch((error) => {
      console.error(
        "Erreur lors de la récupération des informations du patient :",
        error
      );
    });
});
