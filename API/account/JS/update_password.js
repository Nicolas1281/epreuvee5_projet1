/* =========================
Gestion modifier mdp
========================= */
function afficherFormulaire() {
  var btnModifierMotDePasse = document.getElementById("btnModifierMotDePasse");
  var formulaireMotDePasse = document.getElementById("formulaireMotDePasse");

  // Masquer le bouton
  btnModifierMotDePasse.style.display = "none";
  // Afficher le formulaire
  formulaireMotDePasse.style.display = "block";
}

function modifierMotDePasse() {
  var newPassword = document.getElementById("newPassword").value;
  var confirmPassword = document.getElementById("confirmPassword").value;

  // Vérifier si les champs sont vides
  if (newPassword === "" || confirmPassword === "") {
    alert("Veuillez remplir tous les champs.");
    return;
  }

  // Vérifier si le nouveau mot de passe correspond à la confirmation
  if (newPassword !== confirmPassword) {
    alert("Le nouveau mot de passe ne correspond pas à la confirmation.");
    return;
  }

  // Construire l'objet à envoyer dans la requête
  var updateData = {
    password: newPassword,
  };

  // Envoyer la requête de mise à jour du mot de passe à l'API
  fetch("http://milmedcare/API/account/update.php?action=update", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(updateData),
  })
    .then((response) => response.json())
    .then((data) => {
      // Traiter la réponse de l'API ici
      console.log(data);

      // Fermer le formulaire ou effectuer d'autres actions nécessaires après la modification du mot de passe
      var formulaireMotDePasse = document.getElementById(
        "formulaireMotDePasse"
      );
      formulaireMotDePasse.style.display = "none";
    })
    .catch((error) => {
      console.error("Erreur lors de la mise à jour du mot de passe:", error);

      // Gérer les erreurs ici (afficher un message d'erreur, par exemple)
      alert(error.message || "Erreur lors de la modification du mot de passe.");
    });
}
