<?php
require_once("../db_connect.php");
require_once('../config.php');

function getUserData($conn, $userID) {
    $query = "SELECT * FROM account WHERE ID = $userID";
    $result = $conn->query($query);

    if ($result) {
        if ($result->num_rows > 0) {
            $row = $result->fetch_assoc();
            return [
                "userFullName" => $row['Nom'],
                "userFirstName" => $row['Prenom'],
                "dateOfBirth" => $row['DateNaissance'],
                "gender" => $row['gender'],
                "bloodGroup" => $row['blood_group'],
                "allergies" => $row['Allergie'],
                "address" => $row['Adresse'],
                "postalCode" => $row['CodePostal'],
                "city" => $row['LieuxAffectation'],
                "phone" => $row['Telephone'],
                "email" => $row['email'],
                "socialSecurityNumber" => $row['Social_Security_Number']
            ];
        } else {
            return null;
        }
    } else {
        return null;
    }
}

if ($_SERVER["REQUEST_METHOD"] === "GET") {
    if (isset($_GET['id'])) {
        $userID = $_GET['id'];
        $userData = getUserData($conn, $userID);

        if ($userData) {
            echo json_encode($userData);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucun enregistrement trouvé"]);
        }
    } elseif (isset($_GET['action']) && $_GET['action'] === 'read' && isset($_SESSION['userID'])) {
        $userID = $_SESSION['userID'];
        $userData = getUserData($conn, $userID);

        if ($userData) {
            echo json_encode($userData);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucun enregistrement trouvé"]);
        }
    } else {
        http_response_code(400);
        echo json_encode(["message" => "Paramètres invalides"]);
    }
} else {
    http_response_code(405);
    echo json_encode(["message" => "Méthode non autorisée"]);
}
