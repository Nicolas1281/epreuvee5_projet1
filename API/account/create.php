<?php
require_once("../db_connect.php");
require_once('../config.php');

/*ROUTE API*/

/* Création de compte utilisateur */
if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_GET['action']) && $_GET['action'] === 'create') {
    // Vérifier si le formulaire est soumis avec des données
    if (!empty($_POST["username"]) && !empty($_POST["firstname"]) && !empty($_POST["codepostal"]) && !empty($_POST["address"]) && !empty($_POST["choix"]) && !empty($_POST["email"]) && !empty($_POST["phone"]) && !empty($_POST["birthdate"]) && !empty($_POST["password"])) {
        // Récupération des données du formulaire
        $username = $_POST["username"];
        $firstname = $_POST["firstname"];
        $codepostal = $_POST["codepostal"];
        $address = $_POST["address"];
        $lieuxAffectation = $_POST["choix"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $birthdate = $_POST["birthdate"];
        $password = $_POST["password"];

        // Hash du mot de passe en SHA-512
        $hashedPassword = hash('sha512', $password);

        // Insertion des données dans la base de données
        $sql = "INSERT INTO account (Nom, Prenom, CodePostal, Adresse, LieuxAffectation, email, Telephone, DateNaissance, password, created, modified) 
                VALUES ('$username', '$firstname', '$codepostal', '$address', '$lieuxAffectation', '$email', '$phone', '$birthdate', '$hashedPassword', NOW(), NOW())";

        if ($conn->query($sql) === TRUE) {
            // Réponse JSON en cas de succès
            echo json_encode(["success" => true, "message" => "Compte créé avec succès."]);
        } else {
            // Réponse JSON en cas d'erreur
            echo json_encode(["success" => false, "message" => "Erreur d'inscription: " . $conn->error]);
        }
        // Ne pas effectuer de redirection
    } else {
        // Réponse JSON si le formulaire est soumis avec des données vides
        echo json_encode(["success" => false, "message" => "Toutes les données du formulaire doivent être remplies"]);
    }
} else {
    // Réponse JSON pour une mauvaise requête
    echo json_encode(["success" => false, "message" => "Action non valide ou méthode non autorisée"]);
}
