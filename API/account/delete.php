<?php
require_once("../db_connect.php");
require_once('../config.php');

// Vérifier si l'utilisateur est connecté
if (!isset($_SESSION['userID'])) {
    http_response_code(401);
    echo json_encode(["message" => "Vous devez être connecté pour effectuer cette action"]);
    exit();
}

if ($_SERVER["REQUEST_METHOD"] === "DELETE") {
    // Récupérer l'ID du compte à supprimer depuis les paramètres
    $userID = $_SESSION['userID'] ?? null;
    if ($userID === null) {
        http_response_code(400);
        echo json_encode(["message" => "Paramètres invalides"]);
        exit();
    }

    // Vérifiez que l'utilisateur est autorisé à supprimer ce compte (s'il ne s'agit pas de son propre compte)
    $loggedInUserID = $_SESSION['userID'];
    if ($loggedInUserID != $userID) {
        http_response_code(403);
        echo json_encode(["message" => "Vous n'êtes pas autorisé à supprimer ce compte"]);
        exit();
    }

    // Préparez et exécutez la requête de suppression
    $deleteQuery = "DELETE FROM account WHERE ID = ?";
    $deleteStmt = $conn->prepare($deleteQuery);
    $deleteStmt->bind_param("i", $userID);
    if ($deleteStmt->execute()) {
        // Déconnectez l'utilisateur après la suppression du compte
        session_destroy();

        http_response_code(204); // Réponse de suppression réussie
    } else {
        http_response_code(500);
        echo json_encode(["message" => "Erreur lors de l'exécution de la requête : " . $deleteStmt->error]);
    }
} else {
    http_response_code(405);
    echo json_encode(["message" => "Méthode non autorisée"]);
}
