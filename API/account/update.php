<?php
require_once("../db_connect.php");
require_once('../config.php');


// Vérifier si l'utilisateur est connecté
if (!isset($_SESSION['userID'])) {
    http_response_code(401);
    echo json_encode(["message" => "Vous devez être connecté pour effectuer cette action"]);
    exit();
}

if ($_SERVER["REQUEST_METHOD"] === "PUT") {
    // Récupérer les données du formulaire JSON
    $data = json_decode(file_get_contents("php://input"));

    if (!$data) {
        http_response_code(400);
        echo json_encode(["message" => "Données invalides"]);
        exit();
    }

    $userID = $_SESSION['userID'];

    // Vérifier si le champ 'password' est présent dans les données
    if (isset($data->password)) {
        // Si le champ 'password' est présent, hasher le mot de passe avec SHA-512
        $hashedPassword = hash('sha512', $data->password);

        // Préparez la requête de mise à jour avec le mot de passe
        $updateQuery = "UPDATE account SET password = ? WHERE ID = ?";
        $updateStmt = $conn->prepare($updateQuery);
        $updateStmt->bind_param("si", $hashedPassword, $userID);
    } else {
        // Si le champ 'password' n'est pas présent, exclure le mot de passe de la requête de mise à jour
        $updateQuery = "UPDATE account SET Nom = ?, Prenom = ?, email = ?, Telephone = ?, DateNaissance = ?, gender = ?, blood_group = ?, Allergie = ?, Adresse = ?, CodePostal = ?, LieuxAffectation = ?, Social_Security_Number = ? WHERE ID = ?";
        $updateStmt = $conn->prepare($updateQuery);
        $updateStmt->bind_param("ssssssssssssi", $data->userFullName, $data->userFirstName, $data->email, $data->phone, $data->dateOfBirth, $data->gender, $data->bloodGroup, $data->allergies, $data->address, $data->postalCode, $data->city, $data->socialSecurityNumber, $userID);
    }

    if ($updateStmt->execute()) {
        http_response_code(200); // Réponse de mise à jour réussie
        echo json_encode(["message" => "Profil mis à jour avec succès"]);
    } else {
        http_response_code(500);
        echo json_encode(["message" => "Erreur lors de la mise à jour du profil : " . $updateStmt->error]);
    }
} else {
    http_response_code(405);
    echo json_encode(["message" => "Méthode non autorisée"]);
}
