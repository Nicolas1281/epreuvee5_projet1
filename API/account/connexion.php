<?php
require_once("../db_connect.php");
require_once('../config.php');

/*ROUTE API*/

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_GET['action']) && $_GET['action'] === 'login') {
    if (isset($_POST['email']) && isset($_POST['password'])) {
        // Récupération des données du formulaire
        $email = mysqli_real_escape_string($conn, $_POST['email']);
        $password = $_POST['password'];

        // Requête SQL pour récupérer le mot de passe haché associé à l'email
        $query = "SELECT password, ID FROM account WHERE email = '$email'";
        $result = $conn->query($query);

        if ($result->num_rows == 1) {
            $row = $result->fetch_assoc();
            $hashedPassword = $row['password'];
            $userID = $row['ID'];

            // Hacher le mot de passe saisi par l'utilisateur en SHA-512
            $hashedInputPassword = hash('sha512', $password);

            if ($hashedInputPassword === $hashedPassword) {
                // Mot de passe correct, renvoyer une réponse réussie
                header('Content-Type: application/json');
                $_SESSION['userID'] = $userID; // Stocker l'ID de l'utilisateur dans la session
                echo json_encode(["success" => true, "userID" => $userID]);
                exit();
            } else {
                echo json_encode(["success" => false, "message" => "Email ou mot de passe incorrect."]);
            }
        } else {
            echo json_encode(["success" => false, "message" => "Email ou mot de passe incorrect."]);
        }

        // Fermer la connexion à la base de données
        $conn->close();
    } else {
        echo json_encode(["success" => false, "message" => "Email ou mot de passe manquants."]);
    }
} elseif (isset($_GET['action'])) {
    $action = $_GET['action'];

    if ($action === 'getAccountById' && isset($_GET['id'])) {
        // Code pour récupérer un compte par ID
    } else {
        http_response_code(400);
        echo json_encode(["success" => false, "message" => "Action non valide"]);
    }
} else {
    http_response_code(405);
    echo json_encode(["success" => false, "message" => "Méthode non autorisée"]);
}
