/* =========================
Gestion récuération des diagnostics
========================= */

// Fonction pour créer un conteneur pour chaque diagnostics
function createContainerDiagnostics(diagnostics) {
  // Créer le conteneur principal
  var container = document.createElement("div");
  container.classList.add("mt-5");

  // Ajouter le titre
  var title = document.createElement("h3");
  title.classList.add("card-title", "mt-5", "Colorh3");
  title.textContent = `Diagnostics ${diagnostics.id}`;
  container.appendChild(title);

  // Ajouter le paragraphe pour la Date du diagnostic
  var typeParagraph = document.createElement("p");
  typeParagraph.classList.add("card-text", "mt-3");
  typeParagraph.innerHTML = `<strong>Date du diagnostic:</strong> ${diagnostics.Date}`;
  container.appendChild(typeParagraph);

  // Ajouter le paragraphe pour la maladie diagnostiquée
  var descriptionParagraph = document.createElement("p");
  descriptionParagraph.classList.add("card-text");
  descriptionParagraph.innerHTML = `<strong>Maladie diagnostiquée:</strong> ${diagnostics.Maladie}`;
  container.appendChild(descriptionParagraph);

  // Ajouter le paragraphe pour la description du diagnostic
  var typeParagraph = document.createElement("p");
  typeParagraph.classList.add("card-text", "mt-3");
  typeParagraph.innerHTML = `<strong>Description du diagnostic:</strong> ${diagnostics.Description}`;
  container.appendChild(typeParagraph);

  // Ajouter le paragraphe pour le traitement prescrits
  var descriptionParagraph = document.createElement("p");
  descriptionParagraph.classList.add("card-text");
  descriptionParagraph.innerHTML = `<strong>Traitement prescrits:</strong> ${diagnostics.Traitement}`;
  container.appendChild(descriptionParagraph);

  // Ajouter le séparateur
  var separator = document.createElement("hr");
  container.appendChild(separator);

  // Ajouter le conteneur principal à l'élément ayant l'ID "diagnosticsFormContainer"
  document.getElementById("diagnosticsFormContainer").appendChild(container);
}

// Appeler l'API pour récupérer les diagnostics
fetch("http://milmedcare/API/diagnostics/read.php?action=read_diagnostics")
  .then((response) => {
    if (!response.ok) {
      throw new Error(`Erreur HTTP! Statut: ${response.status}`);
    }
    return response.text(); // Retourne le texte brut de la réponse
  })
  .then((data) => {
    return JSON.parse(data); // Essayez de parser la réponse en JSON
  })
  .then((parsedData) => {
    if (parsedData.length > 0) {
      parsedData.forEach((diagnostics) =>
        createContainerDiagnostics(diagnostics)
      );
    } else {
      document.getElementById("diagnosticsFormContainer").innerHTML =
        "Aucun diagnostics trouvé.";
    }
  })
  .catch((error) =>
    console.error("Erreur lors de la récupération des diagnostics:", error)
  );
