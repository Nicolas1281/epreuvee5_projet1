<?php
require_once("../db_connect.php");
require_once('../config.php');

// Lire les diagnostics médicaux
if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET['action']) && $_GET['action'] === 'read_diagnostics') {
    if (isset($_GET['id'])) {
        // Utiliser l'ID fourni dans la requête
        $userID = $_GET['id'];
        $diagnostics = getDiagnostics($conn, $userID);

        if ($diagnostics) {
            echo json_encode($diagnostics);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucun diagnostic trouvé"]);
        }
    } elseif (isset($_SESSION['userID'])) {
        // Utiliser l'ID stocké dans la session si l'utilisateur est connecté
        $userID = $_SESSION['userID'];
        $diagnostics = getDiagnostics($conn, $userID);

        if ($diagnostics) {
            echo json_encode($diagnostics);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucun diagnostic trouvé"]);
        }
    } else {
        http_response_code(401);
        echo json_encode(["message" => "Non autorisé. Veuillez vous connecter."]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}

// Fonction pour récupérer les diagnostics médicaux
function getDiagnostics($conn, $userID)
{
    // Construisez la requête SQL pour récupérer les diagnostics médicaux associés à cet utilisateur
    $query = "SELECT * FROM diagnostics WHERE account_id = $userID";

    $result = $conn->query($query);

    if ($result) {
        $diagnostics = array(); // Créez un tableau pour stocker les diagnostics

        while ($row = $result->fetch_assoc()) {
            // Ajoutez chaque diagnostic à votre tableau
            $diagnostics[] = array(
                "id" => $row['id'],
                "Date" => $row['Date'],
                "Maladie" => $row['Maladie'],
                "Description" => $row['Description'],
                "Traitement" => $row['Traitement'],
            );
        }

        return $diagnostics;
    } else {
        return null;
    }
}
