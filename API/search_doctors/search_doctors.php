<?php
require_once("../db_connect.php");
require_once('../config.php');

// Recherche de médecins
if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET['action']) && $_GET['action'] === 'search_doctors') {
    if (isset($_GET['speciality']) && isset($_GET['center'])) {
        $specialityId = $_GET['speciality'];
        $centerId = $_GET['center'];

        $doctors = searchDoctors($conn, $specialityId, $centerId);

        if ($doctors) {
            echo json_encode($doctors);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucun médecin trouvé"]);
        }
    } else {
        http_response_code(400);
        echo json_encode(["message" => "Paramètres manquants pour la recherche"]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}

// Fonction pour la recherche de médecins avec le nom de la spécialité et du centre
function searchDoctors($conn, $specialityId, $centerId)
{
    // Construire la requête SQL avec une jointure pour obtenir le nom de la spécialité et du centre
    $query = "SELECT m.*, s.Nom AS speciality_nom, c.centerName AS center_nom
              FROM medecin m
              INNER JOIN speciality s ON m.speciality = s.id
              INNER JOIN center c ON m.center = c.id
              WHERE m.speciality = $specialityId AND m.center = $centerId";

    $result = $conn->query($query);

    if ($result) {
        $doctors = array(); // Créer un tableau pour stocker les médecins

        while ($row = $result->fetch_assoc()) {
            // Ajouter chaque médecin au tableau avec le nom de la spécialité et du centre
            $doctors[] = array(
                "id" => $row['id'],
                "prenom" => $row['prenom'],
                "nom" => $row['nom'],
                "codePostal" => $row['codePostal'],
                "adresse" => $row['adresse'],
                "ville" => $row['ville'],
                "phone" => $row['phone'],
                "center" => $row['center_nom'],
                "speciality" => $row['speciality_nom'],
            );
        }

        return $doctors;
    } else {
        return null;
    }
}
