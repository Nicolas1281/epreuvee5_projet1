<?php
require_once("../db_connect.php");
require_once('../config.php');

// Lire les centres depuis la base de données
if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET['action']) && $_GET['action'] === 'read_center') {
    $query = "SELECT * FROM center";
    $result = $conn->query($query);

    if ($result) {
        $centers = array();

        while ($row = $result->fetch_assoc()) {
            $centers[] = array(
                "id" => $row['id'],
                "centerName" => $row['centerName'],
                "City" => $row['City'],
            );
        }

        header('Content-Type: application/json');
        echo json_encode($centers);
    } else {
        http_response_code(500);
        echo json_encode(["message" => "Erreur lors de la lecture des centres"]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}

$conn->close();
