<?php
require_once("../db_connect.php");
require_once('../config.php');

// Lire les spécialités depuis la base de données
if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET['action']) && $_GET['action'] === 'read_speciality') {
    $query = "SELECT * FROM speciality";
    $result = $conn->query($query);

    if ($result) {
        $specialities = array();

        while ($row = $result->fetch_assoc()) {
            $specialities[] = array(
                "id" => $row['id'],
                "Nom" => $row['Nom'],
            );
        }

        header('Content-Type: application/json');
        echo json_encode($specialities);
    } else {
        http_response_code(500);
        echo json_encode(["message" => "Erreur lors de la lecture des spécialités"]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}

$conn->close();
