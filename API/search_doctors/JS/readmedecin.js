// Fonction pour afficher les résultats de la recherche
function afficherResultats(doctors) {
  // Effacer les résultats précédents si nécessaire
  var resultContainer = document.getElementById("resultContainer");
  resultContainer.innerHTML = "";

  if (doctors.length > 0) {
    doctors.forEach(function (doctor) {
      // Créer des éléments HTML pour chaque médecin et les ajouter au résultat
      var doctorElement = document.createElement("div");
      doctorElement.innerHTML = `
      <div class="card mb-4">
        <div class="card-body">
              <div class="row left">
                  <div class="row">
                      <div class="col-md-3">
                          <i class="fas fa-user"><img src="/assets/img/icons8-médecin-50.png" alt=""></i>
                      </div>
                      <div class="col-md-3">
                          <p class="card-text"><strong>Nom:</strong> ${doctor.nom}</p>
                          <p class="card-text"><strong>Prénom:</strong> ${doctor.prenom}</p>
                          <p class="card-text"><strong>Spécialité:</strong> ${doctor.speciality}</p>
                          <p class="card-text"><strong>Adresse:</strong> ${doctor.adresse}</p>
                      </div>
                      <div class="col-md-3">
                          <p class="card-text"><strong>Code postal:</strong> ${doctor.codePostal}</p>
                          <p class="card-text"><strong>Téléphone:</strong> ${doctor.phone}</p>
                          <p class="card-text"><strong>Email:</strong> ${doctor.email}</p>
                          <p class="card-text"><strong>Hôpital/affectation:</strong> ${doctor.center}</p>
                      </div>
                  </div>
              </div>
              <div class="container mt-5">
                  <div class="row justify-content-center mb-3">
                      <h2 class="lead mb-4 Colorh2 text-center">Réserver un rendez-vous</h2>
                      <div class="d-flex justify-content-center align-items-center">
                          <div>
                              <label for="date">Date:</label>
                              <input type="date" id="date" name="date" class="form-width">
                          </div>
                          <div class="me-5">
                              <label for="time">Heure:</label>
                              <input type="time" id="time" name="time" class="form-width">
                          </div>
                          <div>
                              <button class="btn btn-primary" type="submit">Réserver</button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      `;
      resultContainer.appendChild(doctorElement);
    });
  } else {
    // Aucun médecin trouvé
    resultContainer.innerHTML = "<p>Aucun médecin trouvé.</p>";
  }
}

// Extraire les résultats de l'URL et les afficher
var urlParams = new URLSearchParams(window.location.search);
var doctorsJson = urlParams.get("doctors");
if (doctorsJson) {
  var doctors = JSON.parse(decodeURIComponent(doctorsJson));
  afficherResultats(doctors);
}
