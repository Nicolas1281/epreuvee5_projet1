document.addEventListener("DOMContentLoaded", function () {
  // Charger les centres dans le premier menu déroulant
  var centerSelect = document.getElementById("centerSelect");
  loadOptions(
    "http://milmedcare/API/search_doctors/read_center.php?action=read_center",
    centerSelect
  );

  // Charger les spécialités dans le deuxième menu déroulant
  var specialitySelect = document.getElementById("specialitySelect");
  loadOptions(
    "http://milmedcare/API/search_doctors/read_speciality.php?action=read_speciality",
    specialitySelect
  );
});

// Fonction pour charger les options dans un menu déroulant
function loadOptions(apiUrl, selectElement) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", apiUrl, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4 && xhr.status === 200) {
      var options = JSON.parse(xhr.responseText);
      populateOptions(options, selectElement);
    }
  };
  xhr.send();
}

// Fonction pour peupler les options dans un menu déroulant
function populateOptions(options, selectElement) {
  options.forEach(function (option) {
    var optionElement = document.createElement("option");
    optionElement.value = option.id;
    optionElement.text = option.centerName || option.Nom; // Choisir la propriété appropriée
    selectElement.add(optionElement);
  });
}
