function rechercher() {
  var selectedCenter = document.getElementById("centerSelect").value;
  var selectedSpeciality = document.getElementById("specialitySelect").value;

  // Appel à l'API de recherche de médecins
  var apiUrl =
    "http://milmedcare/API/search_doctors/search_doctors.php?action=search_doctors";
  apiUrl += "&speciality=" + selectedSpeciality + "&center=" + selectedCenter;

  var xhr = new XMLHttpRequest();
  xhr.open("GET", apiUrl, true);
  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        var doctors = JSON.parse(xhr.responseText);
        // Redirection vers search.php avec les résultats en tant que paramètre JSON
        window.location.href =
          "../views/search.php?doctors=" +
          encodeURIComponent(JSON.stringify(doctors));
      } else {
        console.error("Erreur lors de la recherche de médecins");
        // Gérer les erreurs si nécessaire
      }
    }
  };
  xhr.send();
}
