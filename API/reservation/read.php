<?php
require_once("../db_connect.php");
require_once('../config.php');

// Lire les réservations
if ($_SERVER["REQUEST_METHOD"] === "GET" && isset($_GET['action']) && $_GET['action'] === 'read_reservation') {
    if (isset($_GET['id'])) {
        // Utiliser l'ID fourni dans la requête
        $userID = $_GET['id'];
        $reservations = getReservations($conn, $userID);

        if ($reservations) {
            echo json_encode($reservations);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucune réservation trouvée"]);
        }
    } elseif (isset($_SESSION['userID'])) {
        // Utiliser l'ID stocké dans la session si l'utilisateur est connecté
        $userID = $_SESSION['userID'];
        $reservations = getReservations($conn, $userID);

        if ($reservations) {
            echo json_encode($reservations);
        } else {
            http_response_code(404);
            echo json_encode(["message" => "Aucune réservation trouvée"]);
        }
    } else {
        http_response_code(401);
        echo json_encode(["message" => "Non autorisé. Veuillez vous connecter."]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}

// Fonction pour récupérer les réservations
function getReservations($conn, $userID)
{
    // Construire la requête SQL pour récupérer les réservations associées à cet utilisateur
    $query = "SELECT * FROM reservation WHERE account_id = $userID";

    $result = $conn->query($query);

    if ($result) {
        $reservations = array(); // Créer un tableau pour stocker les réservations

        while ($row = $result->fetch_assoc()) {
            // Ajouter chaque réservation à votre tableau
            $reservations[] = array(
                "reservation_id" => $row['reservation_id'],
                "date" => $row['date'],
                "heure" => $row['heure'],
                "medecin" => $row['medecin'],
                "speciality" => $row['speciality'],
                "center" => $row['center'],
            );
        }

        return $reservations;
    } else {
        return null;
    }
}
