<?php
require_once("../db_connect.php");
require_once('../config.php');

// Mettre à jour la réservation
if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['action']) && $_POST['action'] === 'update_reservation') {
    // Vérifier si tous les paramètres nécessaires ont été fournis
    if (isset($_POST['reservation_id']) && isset($_POST['date']) && isset($_POST['heure'])) {
        $reservationID = $_POST['reservation_id'];
        $newDate = $_POST['date'];
        $newHeure = $_POST['heure'];

        // Construire la requête SQL pour mettre à jour la réservation dans la base de données
        $query = "UPDATE reservation SET date = ?, heure = ? WHERE id = ?";

        // Créer une instruction préparée
        $stmt = $conn->prepare($query);

        // Vérifier la préparation de l'instruction
        if ($stmt) {
            // Lier les paramètres à l'instruction préparée
            $stmt->bind_param("ssi", $newDate, $newHeure, $reservationID);

            // Exécuter l'instruction préparée
            if ($stmt->execute()) {
                http_response_code(200);
                echo json_encode(["message" => "Réservation mise à jour avec succès"]);
            } else {
                http_response_code(500);
                echo json_encode(["message" => "Erreur lors de l'exécution de la requête : " . $stmt->error]);
            }

            // Fermer l'instruction préparée
            $stmt->close();
        } else {
            http_response_code(500);
            echo json_encode(["message" => "Erreur dans la préparation de l'instruction: " . $conn->error]);
        }
    } else {
        http_response_code(400);
        echo json_encode(["message" => "Paramètres manquants"]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}
