<?php
require_once("../db_connect.php");
require_once('../config.php');

// Créer une réservation
if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['action']) && $_POST['action'] === 'create_reservation') {
    // Vérifier si l'utilisateur est connecté
    if (isset($_SESSION['userID'])) {
        // Récupérer l'ID du compte à partir de la session
        $account_id = $_SESSION['userID'];

        // Validation des données
        if (empty($_POST['date']) || empty($_POST['heure']) || empty($_POST['medecin']) || empty($_POST['speciality']) || empty($_POST['center'])) {
            http_response_code(400);
            echo json_encode(["error" => "Tous les champs sont obligatoires pour la réservation."]);
            exit();
        }

        // Requête SQL avec une instruction préparée pour insérer des données dans la base de données
        $sql = "INSERT INTO reservation (date, heure, medecin, speciality, center, account_id) VALUES (?, ?, ?, ?, ?, ?)";

        // Créer une instruction préparée
        $stmt = $conn->prepare($sql);

        // Vérifier la préparation de l'instruction
        if (!$stmt) {
            http_response_code(500);
            echo json_encode(["error" => "Erreur dans la préparation de l'instruction: " . $conn->error]);
            exit();
        }

        // Lier les paramètres à l'instruction préparée
        $stmt->bind_param("sssssi", $_POST['date'], $_POST['heure'], $_POST['medecin'], $_POST['speciality'], $_POST['center'], $account_id);

        // Exécuter l'instruction préparée
        if ($stmt->execute()) {
            echo json_encode(["message" => "Réservation créée avec succès."]);
        } else {
            http_response_code(500);
            echo json_encode(["error" => "Erreur lors de la création de la réservation: " . $stmt->error]);
        }

        // Fermer l'instruction préparée
        $stmt->close();
    } else {
        http_response_code(401);
        echo json_encode(["message" => "Non autorisé. Veuillez vous connecter."]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Mauvaise requête. Action manquante ou invalide."]);
}

// Fermer la connexion à la base de données
$conn->close();
