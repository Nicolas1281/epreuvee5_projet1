<?php
require_once("../db_connect.php");
require_once('../config.php');

// Endpoint pour supprimer un rendez-vous
if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST['action']) && $_POST['action'] === 'delete_reservation') {
    // Vérifiez si un ID de réservation a été fourni dans la requête
    if (isset($_POST['reservation_id'])) {
        $reservationID = $_POST['reservation_id'];

        // Construisez la requête SQL pour supprimer la réservation de la base de données
        $query = "DELETE FROM reservation WHERE reservation_id = $reservationID";

        if ($conn->query($query) === TRUE) {
            http_response_code(200);
            echo json_encode(["message" => "Réservation supprimée avec succès"]);
        } else {
            http_response_code(500);
            echo json_encode(["message" => "Erreur lors de l'exécution de la requête : " . $conn->error]);
        }
    } else {
        http_response_code(400);
        echo json_encode(["message" => "Paramètre 'reservation_id' manquant"]);
    }
} else {
    http_response_code(400);
    echo json_encode(["message" => "Paramètre 'action' manquant ou invalide"]);
}
