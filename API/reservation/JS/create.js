/* =============================================
Appeler l'API pour créer une réservation
================================================ */

$(document).ready(function () {
  // Lorsque le bouton "Réserver" est cliqué
  $(".btn-primary").on("click", function () {
    console.log("Bouton Réserver cliqué");
    // Récupérer les données nécessaires à partir de la carte
    var doctor = $(this).closest(".card-body").find(".row.left");
    var nom = doctor.find("p:eq(0)").text().split(": ")[1];
    var prenom = doctor.find("p:eq(1)").text().split(": ")[1];
    var speciality = doctor.find("p:eq(2)").text().split(": ")[1];
    var center = doctor.find("p:eq(7)").text().split(": ")[1];

    console.log("Données du médecin :");
    console.log("nom :", nom);
    console.log("prenom :", prenom);
    console.log("Spécialité :", speciality);
    console.log("Hôpital/affectation :", center);

    // Récupérer la date et l'heure saisies par l'utilisateur
    var date = $("#date").val();
    var time = $("#time").val();

    console.log("Date :", date);
    console.log("Heure :", time);

    // Envoyer une requête AJAX pour créer la réservation
    $.ajax({
      type: "POST",
      url: "http://milmedcare/API/reservation/create.php",
      data: {
        action: "create_reservation",
        date: date,
        heure: time,
        medecin: nom + " " + prenom,
        speciality: speciality,
        center: center,
      },
      success: function (response) {
        console.log("Réponse de l'API de réservation :", response);
        // Rafraîchir la page après le succès
        location.reload();
      },
      error: function (error) {
        console.log("Erreur lors de l'appel de l'API de réservation :", error);
        // Gérer les erreurs
      },
    });
  });
});
