/* =======================================================
Appeler l'API pour récupérer les réservations
======================================================= */

fetch("http://milmedcare/API/reservation/read.php?action=read_reservation")
  .then((response) => {
    if (!response.ok) {
      throw new Error(`Erreur HTTP! Statut: ${response.status}`);
    }
    return response.json(); // Retourne la réponse en tant qu'objet JSON
  })
  .then((data) => {
    if (data.length > 0) {
      data.forEach((reservation) => createReservationCard(reservation));
    } else {
      document.getElementById("reservationContainer").innerHTML =
        "Aucune réservation trouvée.";
    }
  })
  .catch((error) =>
    console.error("Erreur lors de la récupération des réservations:", error)
  );

// Fonction pour créer une carte de réservation
function createReservationCard(reservation) {
  // Créer un élément de carte
  var card = document.createElement("div");
  card.classList.add("col-md-4", "mb-4"); // Ajout de la classe "mb-4" pour ajouter un espacement entre les cartes

  // Vérifier si la réservation est passée ou à venir
  var reservationDateTime = new Date(
    reservation.date + " " + reservation.heure
  );
  var currentDateTime = new Date();

  // Ajouter la classe CSS en fonction du statut de la réservation
  var statusClass =
    reservationDateTime < currentDateTime ? "passed" : "upcoming";

  // Déterminer la couleur de fond en fonction de la classe de statut
  var backgroundColor =
    statusClass === "passed" ? "rgba(255, 0, 0, 0.1)" : "rgba(0, 255, 0, 0.1)";

  card.innerHTML = `
    <div class="card">
        <div class="card-body ${statusClass} rounded" style="background-color: ${backgroundColor};">
            <h3 class="card-title Colorh3">Rendez-vous</h3>
            <p class="card-text mt-5"><strong>Date:</strong> ${reservation.date}</p>
            <p class="card-text"><strong>Heure:</strong> ${reservation.heure}</p>
            <p class="card-text"><strong>Médecin:</strong> ${reservation.medecin}</p>
            <p class="card-text"><strong>Spécialité:</strong> ${reservation.speciality}</p>
            <p class="card-text"><strong>Hôpital militaire:</strong> ${reservation.center}</p>
            <div class="col-6 mx-auto">
                <button class="btn btn-outline-success" type="button">Modifier Réservation</button>
                <button class="btn btn-outline-danger mt-3" type="button" data-reservation-id="ID_DE_LA_RESERVATION">Annuler Réservation</button>
            </div>
        </div>
    </div>
  `;

  // Ajouter la carte au conteneur des réservations
  document.getElementById("reservationContainer").appendChild(card);
}
