<?php
require_once('../includes/header.php');
require_once('../includes/navbarDash.php');
?>

<div class="text-center pad pb-5">
    <h1 class="display-5 fw-bold Colorh1">Profil</h1>
</div>

<div class="text-start pad pb-5">
    <h2 class="display-5 fw-bold Colorh2 mb-3">Bonjour <span id="userFirstName"></span></h2>
    <p class="lead mb-4 colorp">Bienvenue sur la page de votre profil personnel. Ici, vous pouvez visualiser et gérer toutes vos informations
        personnelles et médicales. Votre Profil est confidentiel et accessible uniquement par vous et les professionnels de santé autorisés.
    </p>
</div>

<div class="card mb-4">
    <div class="card-body">
        <h2 class="card-title Colorh2"><i class="fas fa-user"><img src="/assets/img/icons8-carte-d'identité.png" alt=""></i> Informations du patient</h2>

        <!-- HTML pour afficher les informations du profil et le bouton de modification -->
        <div id="profileInfo">
            <div class="row">
                <div class="col-md-6 left">
                    <p class="card-text mt-5"><strong>Nom:</strong> <span id="userFullName"></span></p>
                    <p class="card-text"><strong>Prénom:</strong> <span id="userFirstName2"></span></p>
                    <p class="card-text"><strong>Date de naissance:</strong> <span id="dateOfBirth"></span></p>
                    <p class="card-text"><strong>Sexe:</strong> <span id="gender"></span></p>
                    <p class="card-text"><strong>Groupe sanguin:</strong> <span id="bloodGroup"></span></p>
                    <p class="card-text"><strong>Allergies:</strong> <span id="allergies"></span></p>
                </div>
                <div class="col-md-6">
                    <p class="card-text mt-5"><strong>Adresse:</strong> <span id="address"></span></p>
                    <p class="card-text"><strong>Code postal:</strong> <span id="postalCode"></span></p>
                    <p class="card-text"><strong>Lieux Affectation:</strong> <span id="city"></span></p>
                    <p class="card-text"><strong>Téléphone:</strong> <span id="phone"></span></p>
                    <p class="card-text"><strong>Email:</strong> <span id="email"></span></p>
                    <p class="card-text"><strong>Numéro de sécurité sociale:</strong> <span id="socialSecurityNumber"></span></p>
                </div>
            </div>
            <div class="text-center mt-5">
                <button id="editProfileBtn" class="btn btn-color">Modifier le profil</button>
            </div>
        </div>
        <!-- HTML pour afficher le formulaire de modification du profil -->
        <div id="profileForm" style="display: none;">
            <h2 class="display-5 fw-bold Colorh2 mb-3 text-center mt-5">Modifier le profil</h2>
            <form id="profileUpdateForm" class="row">
                <div class="col-md-6 left">
                    <div class="mb-3">
                        <label for="newUserFullName" class="form-label">Nom:</label>
                        <input type="text" class="form-control" id="newUserFullName" name="newUserFullName">
                    </div>
                    <div class="mb-3">
                        <label for="newUserFirstName" class="form-label">Prénom:</label>
                        <input type="text" class="form-control" id="newUserFirstName" name="newUserFirstName">
                    </div>
                    <div class="mb-3">
                        <label for="newDateOfBirth" class="form-label">Date de naissance:</label>
                        <input type="date" class="form-control" id="newDateOfBirth" name="newDateOfBirth">
                    </div>
                    <div class="mb-3">
                        <label for="newGender" class="form-label">Sexe:</label>
                        <input type="text" class="form-control" id="newGender" name="newGender">
                    </div>
                    <div class="mb-3">
                        <label for="newBloodGroup" class="form-label">Groupe sanguin:</label>
                        <input type="text" class="form-control" id="newBloodGroup" name="newBloodGroup">
                    </div>
                    <div class="mb-3">
                        <label for="newAllergies" class="form-label">Nouvelles allergies:</label>
                        <input type="text" class="form-control" id="newAllergies" name="newAllergies">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label for="newAddress" class="form-label">Adresse:</label>
                        <input type="text" class="form-control" id="newAddress" name="newAddress">
                    </div>
                    <div class="mb-3">
                        <label for="newPostalCode" class="form-label">Code postal:</label>
                        <input type="text" class="form-control" id="newPostalCode" name="newPostalCode">
                    </div>
                    <div class="mb-3">
                        <label for="newCity" class="form-label">Lieux Affectation:</label>
                        <input type="text" class="form-control" id="newCity" name="newCity">
                    </div>
                    <div class="mb-3">
                        <label for="newPhone" class="form-label">Téléphone:</label>
                        <input type="text" class="form-control" id="newPhone" name="newPhone">
                    </div>
                    <div class="mb-3">
                        <label for="newEmail" class="form-label">Email:</label>
                        <input type="text" class="form-control" id="newEmail" name="newEmail">
                    </div>
                    <div class="mb-3">
                        <label for="newSocialSecurityNumber" class="form-label">Numéro de sécurité sociale:</label>
                        <input type="text" class="form-control" id="newSocialSecurityNumber" name="newSocialSecurityNumber">
                    </div>
                </div>
                <div class="text-center mt-5">
                    <button type="submit" class="btn btn-color">Enregistrer les modifications</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="card mb-4">
    <div class="card-body">
        <h2 class="card-title Colorh2 left">Antécédents Médicaux</h2>

        <div class="antecedents-list left">
            <!-- Zone pour afficher les antécédents existants -->
            <div id="antecedentFormContainer"></div>
        </div>

        <!-- Votre bouton HTML -->
        <div class="text-center mt-5">
            <button class="btn-color" id="addAntecedentBtn" type="button">Ajouter un antécédent</button>
        </div>

        <!-- Votre formulaire Bootstrap (invisible par défaut) -->
        <div class="container left mt-4" id="antecedentForm" style="display: none;">
            <form id="createAntecedentForm">
                <!-- Vos champs de formulaire ici -->
                <div class="form-group">
                    <label for="antecedentType">Type d'antécédent:</label>
                    <input type="text" class="form-control mt-2" id="antecedentType" placeholder="Type d'antécédent">
                </div>

                <div class="form-group mt-2">
                    <label for="antecedentDescription">Description d'antécédent:</label>
                    <input type="text" class="form-control mt-2" id="antecedentDescription" placeholder="Description d'antécédent">
                </div>

                <!-- Boutons Enregistrer et Annuler -->
                <div class="form-group mt-3">
                    <button type="submit" class="btn btn-success" id="saveAntecedentBtn">Enregistrer</button>
                    <button type="button" class="btn btn-secondary" id="cancelAntecedentBtn">Annuler</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="text-start pad">
    <div class="g-3 py-5">
        <h2 class="Colorh2 text-start mb-3">Sécurité</h2>
        <p class="lead mb-4 colorp text-start">Pour des raisons de confidentialité et de sécurité, veuillez à ne jamais partager votre identifiant, mot de passe
            et autres informations sensibles avec d’autres personnes. Si vous souhaitez changer votre mot de passe, utilisez la fonction “Changer mot de passe”
            ci-dessous.
        </p>
        <h2 class="Colorh2 text-start mb-3">Changer de mot de passe</h2>
        <p class="lead mb-4 colorp text-start">Si vous souhaitez changer votre mot de passe, veuillez utiliser cette fonction pour créer un nouveau mot de passe
            sécurisé.
        </p>
        <div class="mt-3">
            <div id="btnModifierMotDePasse" class="text-start my-3">
                <button class="btn-color" type="button" onclick="afficherFormulaire()">Modifier votre mot de passe</button>
            </div>

            <div id="formulaireMotDePasse" style="display: none;">
                <!-- Formulaire de modification de mot de passe -->
                <form id="formMotDePasse">
                    <div class="mb-3">
                        <label for="newPassword" class="form-label">Nouveau mot de passe</label>
                        <input type="password" class="form-mdp" id="newPassword" name="newPassword" required>
                    </div>

                    <div class="mb-3">
                        <label for="confirmPassword" class="form-label">Confirmer le nouveau mot de passe</label>
                        <input type="password" class="form-mdp" id="confirmPassword" name="confirmPassword" required>
                    </div>

                    <button type="button" class="btn-color" onclick="modifierMotDePasse()">Modifier le mot de passe</button>
                </form>
            </div>
        </div>
        <h2 class="Colorh2 text-start mb-3">Supprimer compte</h2>
        <p class="lead mb-4 colorp text-start">Si vous souhaitez supprimer votre compte MilMedCare, veuillez utiliser cette fonction ci dessous. Veuillez noter
            que cette action est irréversible et toutes vos données seront définitivement effacés.
        </p>
        <div class="text-start my-3">
            <button class="btn-color" type="button" id="deleteAccountButton">Supprimer votre compte</button>
        </div>
        <!-- Modal de confirmation -->
        <div class="modal fade" id="deleteConfirmationModal" tabindex="-1" role="dialog" aria-labelledby="deleteConfirmationModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="deleteConfirmationModalLabel">Confirmation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Êtes-vous sûr de vouloir supprimer votre compte ?</p>
                    </div>
                    <div class="modal-footer">
                        <button id="confirmDeleteButton" class="btn btn-danger" type="button">Confirmer</button>
                        <button id="cancelDeleteButton" class="btn btn-secondary" type="button" data-bs-dismiss="modal">Annuler</button>
                    </div>
                </div>
            </div>
        </div>
        <h2 class="Colorh2 text-start mb-3">Besoin d’aide ?</h2>
        <p class="lead mb-4 colorp text-start">Si vous rencontrer des problèmes d’accès à votre compte ou si vous avez des questions concernant la gestion de
            votre profil, n’hésitez pas à contacter notre équipe d’assistance.
        </p>
    </div>
</div>

<script src="../API/account/JS/read.js"></script>
<script src="../API/account/JS/update_profile.js"></script>
<script src="../API/account/JS/update_password.js"></script>
<script src="../API/account/JS/delete.js"></script>

<script src="../API/antecedent/JS/create.js"></script>
<script src="../API/antecedent/JS/read_profile.js"></script>
<script src="../API/antecedent/JS/update.js"></script>
<script src="../API/antecedent/JS/delete.js"></script>

<?php require_once('../includes/footerDash.php'); ?>