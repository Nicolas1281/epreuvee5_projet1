<?php
require_once('../includes/header.php');
require_once('../includes/navbarDash.php');
?>

<div class="container">
    <h1 class="Colorh1">Conditions d'Utilisation</h1>
    <p class="colorp"><strong>Dernière mise à jour :</strong> [Date de la dernière mise à jour]</p>

    <p class="colorp">En utilisant le site web MilMedCare, vous acceptez de vous conformer à ces conditions d'utilisation. Si vous n'acceptez pas ces conditions, veuillez ne pas utiliser notre site.</p>

    <h2 class="Colorh2">Utilisation du Site</h2>
    <p class="colorp">Vous acceptez d'utiliser le site MilMedCare uniquement à des fins légales et conformément à ces conditions d'utilisation. Vous ne devez pas utiliser notre site de manière frauduleuse, nuisible, illégale ou contraire à l'éthique.</p>

    <h2 class="Colorh2">Protection des Informations d'Utilisateur</h2>
    <p class="colorp">Lors de l'utilisation de notre site, vous devez fournir des informations d'utilisateur exactes et à jour. Vous êtes responsable de la confidentialité de vos informations de compte et de tout mot de passe associé.</p>

    <h2 class="Colorh2">Propriété Intellectuelle</h2>
    <p class="colorp">Le contenu et les éléments du site MilMedCare, y compris les textes, images, logos, marques de commerce et tout autre matériel, sont protégés par des droits de propriété intellectuelle. Vous ne pouvez pas copier, modifier, distribuer, afficher, reproduire ou utiliser de quelque manière que ce soit le contenu sans autorisation préalable.</p>

    <h2 class="Colorh2">Liens Vers des Tiers</h2>
    <p class="colorp">Notre site peut contenir des liens vers des sites tiers. Nous ne sommes pas responsables du contenu ou des pratiques de confidentialité de ces sites. L'utilisation de ces liens est à vos propres risques.</p>

    <h2 class="Colorh2">Limitation de Responsabilité</h2>
    <p class="colorp">Le site MilMedCare est fourni "tel quel" et sans garantie d'aucune sorte. Nous ne pouvons garantir que le site sera toujours disponible, sans erreurs ou exempt de virus ou d'autres éléments nuisibles. Nous ne serons pas responsables des dommages directs, indirects, spéciaux, accessoires ou consécutifs résultant de l'utilisation de notre site.</p>

    <h2 class="Colorh2">Modifications des Conditions d'Utilisation</h2>
    <p class="colorp">Nous pouvons mettre à jour ces conditions d'utilisation de temps en temps. La date de la dernière mise à jour sera indiquée en haut de la page. Veuillez vérifier régulièrement cette page pour vous tenir informé des éventuelles modifications.</p>

    <h2 class="Colorh2">Contact</h2>
    <p class="colorp">Si vous avez des questions ou des préoccupations concernant nos conditions d'utilisation, veuillez nous contacter à <a href="mailto:contact@milmedcare.com">contact@milmedcare.com</a>.</p>
</div>

<?php require_once('../includes/footerDash.php'); ?>