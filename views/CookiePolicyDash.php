<?php
require_once('../includes/header.php');
require_once('../includes/navbarDash.php');
?>

<div class="container">
    <h1 class="Colorh1">Politique de Cookies</h1>
    <p class="colorp"><strong>Dernière mise à jour :</strong> Date de la dernière mise à jour</p>

    <p class="colorp">Cette politique de cookies explique comment nous utilisons les cookies et les technologies similaires sur le site web MilMedCare. En utilisant notre site, vous consentez à l'utilisation de cookies conformément à cette politique.</p>

    <h2 class="Colorh2">Qu'est-ce qu'un cookie ?</h2>
    <p class="colorp">Un cookie est un petit fichier texte placé sur votre ordinateur ou appareil mobile lorsque vous visitez un site web. Il permet au site web de reconnaître votre appareil et de stocker certaines informations sur vos préférences ou actions passées.</p>

    <h2 class="Colorh2">Comment utilisons nous les cookies ?</h2>
    <p class="colorp">Nous utilisons des cookies pour diverses raisons, notamment pour :</p>
    <ul>
        <li>Vous permettre de naviguer efficacement sur notre site et d'accéder à ses fonctionnalités.</li>
        <li>Améliorer la performance de notre site en enregistrant des informations sur les pages que vous visitez et les liens sur lesquels vous cliquez.</li>
        <li>Personnaliser votre expérience en vous montrant des contenus pertinents en fonction de vos intérêts.</li>
        <li>Analyser l'utilisation de notre site et obtenir des informations statistiques pour améliorer notre contenu et nos services.</li>
        <li>Fournir des fonctionnalités de médias sociaux intégrées pour vous permettre de partager du contenu sur les réseaux sociaux.</li>
    </ul>

    <h2 class="Colorh2">Types de cookies que nous utilisons</h2>
    <p class="colorp">Nous utilisons à la fois des cookies internes (fournis et gérés par MilMedCare) et des cookies tiers (fournis par des partenaires de confiance) sur notre site. Voici les principaux types de cookies que nous utilisons :</p>
    <ul>
        <li><strong>Cookies de session :</strong> Ces cookies sont temporaires et expirent lorsque vous fermez votre navigateur. Ils sont utilisés pour mémoriser vos actions pendant une session de navigation et ne collectent pas d'informations de manière permanente.</li>
        <li><strong>Cookies persistants :</strong> Ces cookies restent sur votre appareil après la fermeture de votre navigateur et sont utilisés pour mémoriser vos préférences et actions lors de vos visites ultérieures sur notre site.</li>
        <li><strong>Cookies analytiques :</strong> Ces cookies collectent des informations sur l'utilisation de notre site, telles que les pages que vous visitez et les liens sur lesquels vous cliquez. Ces informations nous aident à comprendre comment les utilisateurs interagissent avec notre site et à améliorer son contenu.</li>
        <li><strong>Cookies de fonctionnalités :</strong> Ces cookies permettent à notre site de se souvenir de vos choix et préférences (tels que la langue ou la région) pour vous offrir une expérience plus personnalisée.</li>
    </ul>

    <h2 class="Colorh2">Comment contrôler les cookies</h2>
    <p class="colorp">Vous avez le contrôle sur les cookies que vous acceptez ou refusez. Vous pouvez modifier les paramètres de votre navigateur pour bloquer ou supprimer les cookies. Cependant, veuillez noter que certains cookies sont essentiels au bon fonctionnement de notre site et leur désactivation pourrait affecter votre expérience de navigation.</p>

    <p class="colorp">En continuant à utiliser notre site sans modifier vos paramètres de cookies, vous consentez à notre utilisation des cookies conformément à cette politique.</p>

    <h2 class="Colorh2">Contact</h2>
    <p class="colorp">Si vous avez des questions ou des préoccupations concernant notre politique de cookies, veuillez nous contacter à <a href="mailto:contact@milmedcare.com">contact@milmedcare.com</a>.</p>
</div>

<?php require_once('../includes/footerDash.php'); ?>