<?php
require_once('../includes/header.php');
require_once('../includes/navbarDash.php');
?>

<div class="text-center pad pb-5">
    <h1 class="display-5 fw-bold Colorh1">Médecin disponible</h1>
</div>

<div class="text-start pad pb-5">
    <h2 class="display-5 fw-bold Colorh2 mb-3">Bonjour</h2>
    <p class="lead mb-4 colorp">Nous sommes ravis de vous accueillir sur MilMedCare, votre plateforme de gestion de rendez-vous médicaux. Vous trouvez ci dessous la liste de vos réservations de rendez vous:
    </p>
</div>

<div id="resultContainer">
    <!-- Les résultats de la recherche seront ajoutés ici dynamiquement par JavaScript -->
</div>


<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
        <li class="page-item disabled">
            <a class="page-link">Previous</a>
        </li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
            <a class="page-link" href="#">Next</a>
        </li>
    </ul>
</nav>


<div class="text-start pad">
    <div class="g-3 py-5">
        <h2 class="Colorh2 text-start mb-3">Besoin d’aide ?</h2>
        <p class="lead mb-4 colorp text-start">Si vous avez des questions concernant les médecins disponibles, les spécialités ou le processus de prise de
            rendez-vous, n'hésitez pas à contacter notre équipe d'assistance. Nous sommes là pour vous aider à trouver le meilleur professionnel de santé pour
            répondre à vos besoins médicaux.
            <br><br>
            Nous vous remercions de faire confiance à MilMedcare pour votre suivi médical. Votre santé est notre priorité, et nous sommes déterminés à vous
            offrir les meilleurs soins possibles.
            <br><br>
            Cordialement,<br>
            L'équipe MilMedcare
        </p>
    </div>
</div>

<script src="../API/search_doctors/JS/readmedecin.js"></script>
<script src="../API/reservation/JS/create.js"></script>


<?php require_once('../includes/footerDash.php'); ?>