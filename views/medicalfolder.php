<?php
require_once('../includes/header.php');
require_once('../includes/navbarDash.php');
?>

<div class="text-center pad pb-5">
    <h1 class="display-5 fw-bold Colorh1">Mon dossier médical</h1>
</div>

<div class="text-start pad pb-5">
    <h2 class="display-5 fw-bold Colorh2 mb-3">Bonjour <span id="userFirstName"></span></h2>
    <p class="lead mb-4 colorp">Bienvenue sur la page de votre dossier médicale personnel. Ici, vous pouvez consulter toutes les informations concernant votre
        santé, les diagnostiques, les résultats d’examens, les traitement prescrits et vos antécédents médicaux. Votre dossier médical est confidentiel et
        accessible uniquement par vous et les professionnels de santé autorisés.
    </p>
</div>

<div class="card mb-4">
    <div class="card-body">
        <h2 class="card-title Colorh2"><i class="fas fa-user"><img src="/assets/img/icons8-carte-d'identité.png" alt=""></i> Informations du patient</h2>
        <div class="row">
            <div class="col-md-6 left">
                <p class="card-text mt-5"><strong>Nom:</strong> <span id="userFullName"></span></p>
                <p class="card-text"><strong>Prénom:</strong> <span id="userFirstName2"></span></p>
                <p class="card-text"><strong>Date de naissance:</strong> <span id="dateOfBirth"></span></p>
                <p class="card-text"><strong>Sexe:</strong> <span id="gender"></span></p>
                <p class="card-text"><strong>Groupe sanguin:</strong> <span id="bloodGroup"></span></p>
                <p class="card-text"><strong>Allergies:</strong> <span id="allergies"></span></p>
            </div>
            <div class="col-md-6">
                <p class="card-text mt-5"><strong>Adresse:</strong> <span id="address"></span></p>
                <p class="card-text"><strong>Code postal:</strong> <span id="postalCode"></span></p>
                <p class="card-text"><strong>Lieux Affectation:</strong> <span id="city"></span></p>
                <p class="card-text"><strong>Téléphone:</strong> <span id="phone"></span></p>
                <p class="card-text"><strong>Email:</strong> <span id="email"></span></p>
                <p class="card-text"><strong>Numéro de sécurité sociale:</strong> <span id="socialSecurityNumber"></span></p>
            </div>
        </div>
    </div>
</div>

<div class="card mb-4">
    <div class="card-body">
        <h2 class="card-title Colorh2 left">Antécédents Médicaux</h2>
        <div class="antecedents-list left">
            <!-- Zone pour afficher les antécédents existants -->
            <div id="antecedentFormFolder"></div>
        </div>
    </div>
</div>

<div class="card mb-4">
    <div class="card-body">
        <h2 class="card-title Colorh2 left">Diagnostics et traitements</h2>
        <div class="row left">
            <!-- Zone pour afficher les diagnostics existants -->
            <div id="diagnosticsFormContainer"></div>
        </div>
    </div>
</div>

<div class="card mb-4">
    <div class="card-body">
        <h2 class="card-title Colorh2 left">Examens et résultats</h2>
        <div class="row left">
            <!-- Zone pour afficher les diagnostics existants -->
            <div id="examensFormContainer"></div>
        </div>
    </div>
</div>


<div class="text-start pad">
    <div class="g-3 py-5">
        <h2 class="Colorh2 text-start mb-3">Informations importantes</h2>
        <p class="lead mb-4 colorp text-start">Votre dossier médical est régulièrement mis à jour par votre médecin traitant et les professionnels de santé qui
            vous suivent. <br>
            Si vous avez des questions ou des doutes concernant les informations affichées dans votre dossier médical, n'hésitez pas à en discuter avec votre
            médecin. <br>
            Vous pouvez également mettre à jour vos antécédents médicaux veuillez vous rendre sur votre profil pour modifié vos informations.
            Pour des raisons de confidentialité et de sécurité, veillez à ne jamais partager votre identifiant et mot de passe avec d'autres personnes.
            <br><br>
            Nous vous remercions de votre confiance en MilMedcare. Votre santé est notre priorité, et nous sommes là pour vous accompagner tout au long de votre
            parcours médical.
            <br><br>
            Cordialement,<br>
            L'équipe MilMedcare
        </p>
    </div>
</div>

<script src="../API/account/JS/read.js"></script>
<script src="../API/antecedent/JS/read_folder.js"></script>
<script src="../API/diagnostics/JS/read.js"></script>
<script src="../API/examens/JS/read.js"></script>

<?php require_once('../includes/footerDash.php'); ?>