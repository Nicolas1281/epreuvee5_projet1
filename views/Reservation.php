<?php
require_once('../includes/header.php');
require_once('../includes/navbarDash.php');
?>

<div class="text-center pad pb-5">
    <h1 class="display-5 fw-bold Colorh1">Mes réservations</h1>
</div>

<div class="text-start pad pb-5">
    <h2 class="display-5 fw-bold Colorh2 mb-3">Bonjour</h2>
    <p class="lead mb-4 colorp">Nous sommes ravis de vous accueillir sur MilMedCare, votre plateforme de gestion de rendez-vous médicaux. Vous trouvez ci dessous
        la liste de vos réservations de rendez vous:
    </p>
</div>

<!-- HTML pour le conteneur des réservations -->
<div class="pad">
    <div class="row" id="reservationContainer"></div> <!-- Conteneur pour les réservations -->
</div>

<div class="text-start pad">
    <div class="g-3 py-5">
        <h2 class="Colorh2 text-start mb-3">Informations importantes</h2>
        <p class="lead mb-4 colorp text-start">Vous pouvez consulter vos réservations de rendez-vous à tout moment sur cette page.
            <br><br>
            En cas d'imprévu, nous vous encourageons à annuler votre rendez-vous à l'avance pour permettre à d'autres patients d'en bénéficier.
            <br><br>
            Pour annuler une réservation, cliquez sur le bouton "Annuler Réservation" et suivez les instructions à l'écran. Veuillez noter que certaines
            réservations pourraient être soumises à des politiques d'annulation spécifiques.
            <br><br>
            Si vous avez des questions ou des besoins particuliers concernant vos rendez-vous médicaux, n'hésitez pas à contacter notre service client. Nous
            sommes là pour vous aider !
            <br><br>
            Nous vous remercions de votre confiance en MilMedcare et nous vous souhaitons un prompt rétablissement.
            <br><br>
            Cordialement,<br>
            L'équipe MilMedcare
        </p>
    </div>
</div>

<script src="../API/reservation/JS/read.js"></script>

<?php require_once('../includes/footerDash.php'); ?>