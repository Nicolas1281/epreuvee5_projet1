<?php
require_once('../includes/header.php');
require_once('../includes/navbar.php');
?>

<div class="container">
    <h1 class="Colorh1">Politique de Confidentialité</h1>
    <p class="colorp"><strong>Dernière mise à jour :</strong> [Date de la dernière mise à jour]</p>

    <p class="colorp">Cette politique de confidentialité explique comment nous collectons, utilisons, divulguons et protégeons vos informations personnelles lorsque vous utilisez le site web MilMedCare. En utilisant notre site, vous consentez à la collecte et à l'utilisation de vos informations personnelles conformément à cette politique.</p>

    <h2 class="Colorh2">Collecte des informations personnelles</h2>
    <p class="colorp">Nous pouvons collecter les informations personnelles suivantes lorsque vous utilisez notre site :</p>
    <ul>
        <li>Informations d'identification : nom, prénom, adresse e-mail, numéro de téléphone, etc.</li>
        <li>Informations de santé : antécédents médicaux, traitements, diagnostics, etc.</li>
        <li>Informations de connexion : adresses IP, données de navigation, appareil utilisé, etc.</li>
        <li>Autres informations : préférences, intérêts, commentaires, etc.</li>
    </ul>

    <h2 class="Colorh2">Utilisation des informations personnelles</h2>
    <p class="colorp">Nous utilisons vos informations personnelles pour les finalités suivantes :</p>
    <ul>
        <li>Fournir les services et fonctionnalités de notre site, y compris la prise de rendez-vous médicaux.</li>
        <li>Améliorer l'expérience de l'utilisateur et personnaliser le contenu en fonction de vos intérêts.</li>
        <li>Répondre à vos demandes et vous fournir un support client.</li>
        <li>Analyser l'utilisation de notre site et améliorer son contenu et ses fonctionnalités.</li>
        <li>Vous envoyer des notifications et des mises à jour concernant vos rendez-vous et votre dossier médical.</li>
        <li>Respecter nos obligations légales et réglementaires.</li>
    </ul>

    <h2 class="Colorh2">Partage des informations personnelles</h2>
    <p class="colorp">Nous pouvons partager vos informations personnelles avec les tiers suivants :</p>
    <ul>
        <li>Médecins et professionnels de santé pour faciliter la prise de rendez-vous et la gestion de votre dossier médical.</li>
        <li>Prestataires de services tiers qui nous aident à gérer notre site et à fournir nos services.</li>
        <li>Autorités gouvernementales ou réglementaires en cas de demande légale ou pour se conformer aux lois en vigueur.</li>
    </ul>

    <h2 class="Colorh2">Protection des informations personnelles</h2>
    <p class="colorp">Nous prenons des mesures de sécurité appropriées pour protéger vos informations personnelles contre tout accès, utilisation ou divulgation non autorisés. Cependant, veuillez noter qu'aucune méthode de transmission sur Internet n'est totalement sécurisée.</p>

    <h2 class="Colorh2">Conservation des informations personnelles</h2>
    <p class="colorp">Nous conservons vos informations personnelles aussi longtemps que nécessaire pour fournir nos services et nous conformer à nos obligations légales.</p>

    <h2 class="Colorh2">Vos droits</h2>
    <p class="colorp">Vous avez certains droits concernant vos informations personnelles, notamment le droit d'accéder, de corriger, de supprimer et de limiter l'utilisation de vos données. Pour exercer ces droits, veuillez nous contacter à <a href="mailto:contact@milmedcare.com">contact@milmedcare.com</a>.</p>

    <h2 class="Colorh2">Modifications de la politique de confidentialité</h2>
    <p class="colorp">Nous pouvons mettre à jour cette politique de confidentialité de temps en temps. La date de la dernière mise à jour sera indiquée en haut de la page. Veuillez vérifier régulièrement cette page pour vous tenir informé des éventuelles modifications.</p>

    <h2 class="Colorh2">Contact</h2>
    <p class="colorp">Si vous avez des questions ou des préoccupations concernant notre politique de confidentialité, veuillez nous contacter à <a href="mailto:contact@milmedcare.com">contact@milmedcare.com</a>.</p>
</div>



<?php require_once('../includes/footer.php'); ?>